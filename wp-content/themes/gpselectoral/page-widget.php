<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?php the_title(); ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">



    <!-- Styles -->

    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png?v=2" type="image/x-icon">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css?v=2">

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:100,300,400' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css?v=2">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/layout-multi.css?v=2">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css?v=4">

    <link rel="stylesheet" media="(max-width: 500px)" href="<?php echo get_template_directory_uri(); ?>/css/loader.css?v=2" />

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/vendor/source/jquery.fancybox.css?v=25" type="text/css" media="screen" />



    <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.6.2-respond-1.1.0.min.js?v=2"></script>
	
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54746521-2', 'auto');
  ga('send', 'pageview');

</script>

        <!-- Fav and touch icons -->

	<!--[if lt IE 9]>

	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>

	<![endif]-->

	<?php wp_head(); ?>

  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1199839313379586',
        xfbml      : true,
        version    : 'v2.5'
      });
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  </script>
  <style type="text/css">
 
  div.box-pregunta{
  	min-height: 100%;
    background-size: cover;
    background-position: center 5em;
    height: 550px;
  }
  #container-0.container, .container::after{
  	    height: 100%;
  }
  #container-0 #form{
  	    margin-top: 0;
    padding: 0em 6em 1em 3em;
  }
  .container, .container::after{
  	    height: 100%
  }

  .back-white{
  	height: 70px
  }
  .thumb-nav{
  	top: 5.5em;
  	z-index: 99999999;
  }
  header{}
  .social{
  	margin-top: 15em;
  }
  body{
  	    overflow-y: hidden;
  }
  .logo{
  	max-width: 130px
  }
  .falta{
  	background-color: rgba(17, 17, 17, 0.78);
  }
  .menu{
  	display: none;
  }
  .wrap-question {
    width: 660px;
    margin-left: -330px;
}
  div#nav-icon4.sb-toggle-right{
  	display: none !important;
  }
  #form button{
  	background-color: rgb(17, 17, 17);
  }
  .view-rpts{
  	display: none !important;
  }
  .intro__content {
	    position: absolute;
	    width: 580px;
	    top: 50%;
	    padding: 0em 1em 0 1em;
	    margin: 0 5%;
	    font-size: .95em;
	    background-color: rgba(255, 255, 255, 0.86);
	    box-shadow: 1px 1px 4px 4px rgba(0, 0, 0, 0.27);
	    /* padding-bottom: 2em; */
	    /* height: 100%; */
	    text-align: center;
	    left: 50%;
	    margin-left: -303px;
	    top: 52%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	}
	.intro, .container{
		overflow: visible;
	}
#form .femenino + span {
    font-size: .95em;
}
#form .masculino + span {
    font-size: .95em;
    margin-right: 1.5em;
}
#form label, #form select, #form input[type="text"]{
	    font-size: .9em;
}
#form .genero{
	margin: 0.5em 0;
}
#form input[type="radio"]{
	
}
#form input[type="text"]{
	width: 56.2%;
    margin-bottom: .5em;
}
#form .group-control{
	    padding: 0 0 0;
}
#form select{}
#form > h2 {
    font-size: 1.2em;
    font-weight: 600;
}
footer{
	position: absolute;
    bottom: 0;
    width: 100%;
    padding: 1.5em;
}
  @media screen and (max-width: 1024px) {
  	.container, .container::after{

  	}
  	div.box-pregunta{
	  	min-height: 100%;
	    background-size: cover;
	    background-position: center 5em;
	}
	#container-0.container, .container::after{}
	#container-0 #form{}
	.intro__content{
		width: 480px;
    	margin-left: -240px;
	}
  }
  @media screen and (max-width: 769px) {
  	.container, .container::after{
  		height: 100%;
  	}
  	div.box-pregunta{
	  	min-height: 100%;
	    background-size: cover;
	    background-position: center 5em;
	}
	#container-0.container, .container::after{}
	#container-0 #form{
		padding: 0em 1em 1em 0em;
	}
	.intro__content{
		width: 480px;
    	margin-left: -240px
	}

	
  }
  @media screen and (max-width: 680px) {
  	.thumb-nav{
  		height: 60px;
  		    top: 15%;
  	}
  	.container, .container::after{
  		height: 100%;
  	}
  	div.box-pregunta{
	  	min-height: 100%;
	    background-size: cover;
	    background-position: 25% 4em;
	}
	#container-0.container, .container::after{
		height: 100%;
	}
	.logo{
		    max-width: 120px;
	}
	#container-0 #form{}
	.intro__content{
		width: 75%;
	    margin: 0 10%;
	    left: 0;
	}
	#form input[type="text"]{
		    width: 85.2%;
    		margin-bottom: 0;
	}
	#form label, #form select, #form input[type="text"]{
	    font-size: .7em;
	}
  }
  @media screen and (max-width: 580px){
  	.thumb-nav__item span {
	    line-height: 3.8em;
	    font-size: 1.1em;
	    text-indent: 10px;
	}
  }
  @media screen and (max-width: 480px){
  	.container, .container::after{
  		height: 100%;
  	}
  	div.box-pregunta{
	  	min-height: 100%;
	    background-size: cover;
	    background-position: 25% 4em;
	}
	.fancybox-iframe{
		max-height: 350px;
	}
	.intro__content{
		min-width: 80%;
		width: 80%;
		margin: 0 5%;
	}
  }

  </style>
</head>

<script type="text/javascript">

	function postToFeed(picture, title, summary){
		var obj = {method: 'feed',link: '<?php echo site_url()."/test/"; ?>', picture: picture,name: title, description: summary};
		function callback(response){}
		FB.ui(obj, callback);
	}

</script>

<body <?php body_class(); ?>>

    <div class="sb-slidebar sb-right menu-right">
      <ul>
          <li>
              <a href="#cnd">Candidat@s</a>
          </li>
          <li>
              <a href="">Actvidades</a>
          </li>
          <li>
              <a href="https://www.youtube.com/channel/UCwSFsMYMGfn_DcQae_4FeQg" target="_blank">Videos</a>
          </li>
          <li>
              <a href="http://gpselectoral.pe/test/" target="_self">Test</a>
          </li>
      </ul>
    </div>
    <header>
        <section class="Wrapper">
           <a href="javascript:;"><img src="http://gpselectoral.pe/wp-content/uploads/2016/01/logo_min.png?v=2" alt="Ubica un candidato a tu medida" class="logo"></a>

           <nav class="menu">
                    <ul>
                        <li>
                          <a href="#cnd">Candidat@s</a>
                        </li>
                        <li>
                          <a href="">Actvidades</a>
                        </li>
                        <li>
                          <a href="https://www.youtube.com/channel/UCwSFsMYMGfn_DcQae_4FeQg" target="_blank">Videos</a>
                        </li>
                        <li>
                          <a href="/test/">Test</a>
                        </li>
                    </ul>
           </nav>
            <div id="nav-icon4" class="sb-toggle-right">
              <span></span>
              <span></span>
              <span></span>
            </div>

        </section>
    </header>
    <ul class='social'>
      <li>
        <a class="fa fa-facebook" target="_blank" href="https://www.facebook.com/gpselectoral/?fref=ts">    
          <span>Facebook</span>
        </a> 
      </li>
      <li>
        <a class="fa fa-twitter" target="_blank" href="https://twitter.com/gpselectoral">
          <span>Twitter</span>
        </a>
      </li>
      <li>
        <a class="fa fa-share-alt" onclick="postToFeed('<?php echo get_template_directory_uri(); ?>/img/share-gps.jpg', 'Ubica un candidat@ a tu medida', 'El GPS Electoral es un test en línea que compara tus posiciones sobre temas de interés nacional con las posiciones de los partidos y postulantes a la Presidencia de la República.');" href="javascript:;">
        <span>Compartir</span>
        </a> 
      </li>
    </ul>
    <div id="sb-site">

<div class="box-pregunta open-box-preg">
<div class="wrap-question">
<script src ="<?php echo get_template_directory_uri(); ?>/js/ubigeo.js"></script>


<?php  global $wpdb;

	$departamentos = $wpdb->get_results( 'SELECT id_ubigeo, id_departamento, departamento FROM ubigeo group by id_departamento', OBJECT ); ?>



	 		<span class="back-white"></span>

	 		<?php $j = 1; ?>

	 		<?php 

	            $cant_preguntas = query_posts(array(

	                'post_type' => 'preguntas',

	                'showposts' => 50,

	                'orderby' => 'menu_order',

	                'order'   => 'ASC'

	            ) );  
	            $cant_preguntas = count($cant_preguntas);
	        ?>
<style type="text/css">
	.slick-next:after{
		content: " /<?php echo $cant_preguntas ?>";
	}

</style>
<script>

	var QUESTION_IMPORTANCE = {LOW : 0, MID: 1, HIGH : 2};

	var QUESTION_OPINION = {NOT_AGREE: 'desacuerdo', NEUTRAL : 'neurtal', AGREE : 'de_acuerdo'};

	

	var userData = {departamento_id:"", departamento : "", provincia_id:"", provincia : "", distrito_id : "", distrito : "", genero : "", edad : "",  grado : ""};

	

	var userQuestionResults = {};

	

	var questionElementsAnswered = [];

	var cantidad_total = <?php echo $cant_preguntas; ?>;

	var temp_index_page = 0;

	function skipUbigeo()	

	{

		$('#preg-1 > span').trigger('click');

		$(".thumb-nav").animate({opacity: 1}, 500);

		

	}

	

	function setUserData()

	{
		if ( $('#form').parsley().isValid() ) {

			var form = document.getElementById("form");

			console.log("SET_USER_DATA");

			

			userData.departamento_id = form.departamento.value;

			userData.provincia_id = form.provincia.value;

			userData.distrito_id = form.distrito.value;

			userData.departamento = form.departamento.selectedOptions[0].text;

			userData.provincia = form.provincia.selectedOptions[0].text;

			userData.distrito = form.distrito.selectedOptions[0].text;

			userData.genero = form.genero.value;

			userData.edad = form.edad.value;

			userData.grado = form.grado.value;

			

			$(".thumb-nav").fadeTo();
			$(".box-pregunta").removeClass("open-box-preg");


			console.log(userData);

			$(".thumb-nav").animate({opacity: 1}, 500);
			
		};
			

	}

	

	function saveOpinionForQuestion(questionNumber, opinion, questionId)

	{

		var $imp = parseInt( $("#container-"+questionNumber+" .importancia .active").html() );

		userQuestionResults[questionId] = {answered : true, importance : $imp, opinion : opinion, question_id: questionId};

		questionElementsAnswered[questionNumber] = true;

		//var navigationButton = document.getElementById("preg-" + (questionNumber));

		

		//.thumb-nav__item:hover > span {    background-color: #347F96;

		
		if ( questionNumber < cantidad_total ) {

			

			for(var i = questionNumber + 1; i <= cantidad_total; ++i )

			{

				if( document.getElementById("preg-" + i) != null)

				{

					$('#preg-'+ i + ' > span').trigger('click');


					return $('.thumb-nav').slick('slickNext');

					break;

				}

			}

		};
			
		if ( questionNumber == cantidad_total ) {

			var answeredQuestionsCount = 0;

            if( ObjectLength( userQuestionResults ) < 20)

            {

                alert("Necesita responder " + (20 - answeredQuestionsCount ) +" pregunta(s) más para continuar_/");

                

                return false;

            }

            else

            {   



                $(".thumb-nav").hide(); 

                for(var result in userQuestionResults)

                {

                    delete userQuestionResults[result].answered;

                }

                $(".last-quest").find("#userQuestionResults").val( JSON.stringify(userQuestionResults) );
                $(".last-quest").find("#userData").val( JSON.stringify(userData) );
                

                $(".last-quest").submit();
                console.log("enviar formulario!");
                $(".last-quest").serialize();

            }


		}
			

		

		/*

		if(questionNumber != 29)

  			navigationButton.parentNode.removeChild(navigationButton);

  			*/

	}


	function saveImportanceForQuestion(questionNumber, importance, element)

	{

		console.log(questionNumber, importance);

		

		var importanceOptions = element.parentElement.getElementsByTagName("a");

		

		importanceOptions[0].className = "baja";

		importanceOptions[1].className = "media";

		importanceOptions[2].className = "alta";

		

		element.className += " active";				

	}

		
	function ObjectLength( object ) {
        var length = 0;
        for( var key in object ) {
            if( object.hasOwnProperty(key) ) {
                ++length;
            }
        }
        return length;
    };	

	function finish(form)

	{

		event.preventDefault();

		var answeredQuestionsCount = 0;

		

		/*for(var questionResult in userQuestionResults)

		{

		console.log(questionResult );

			if(userQuestionResults[questionResult].answered) answeredQuestionsCount++;

		}*/

		

		if(answeredQuestionsCount < 20)

		{

			alert("Necesita responder " + (20 - answeredQuestionsCount ) +" pregunta(s) más para continuar");

			

			return false;

		}

		else

		{	



			$(".thumb-nav").hide();	

			for(var result in userQuestionResults)

			{

				delete userQuestionResults[result].answered;

			}

			/*$("form.last-quest").find("#userQuestionResults").val( JSON.stringify(userQuestionResults) );
			$("form.last-quest").find("#userData").val( JSON.stringify(userData) );*/
			
			
			/*var inputUserQuestionResults = document.createElement("input");

			inputUserQuestionResults.name = "userQuestionResults";

			inputUserQuestionResults.type = "hidden"

			inputUserQuestionResults.value = JSON.stringify(userQuestionResults);

			

			var inputUserData = document.createElement("input");

			inputUserData.name = "userData";

			inputUserData.type = "hidden"						

			inputUserData.value = JSON.stringify(userData);*/

			

			/*form.appendChild(inputUserQuestionResults);

			form.appendChild(inputUserData);

			$(inputUserQuestionResults).appendTo(form);

			$(inputUserData).appendTo(form);*/
			

			

			

			//form.submit();		

			

			return true;				

		}

	}
</script>	        

	        <nav class="thumb-nav">

	        	

		        	<a data-container="container-0" class="thumb-nav__item formulario" href="#" title="Registra tus datos"><span>Tus Datos</span></a>

			        <?php while (have_posts()) : the_post(); ?>



			  			<a data-container="container-<?php echo $j; ?>" id="preg-<?php echo $j; ?>" class="thumb-nav__item" href="#" title="<?php the_title(); ?>"><span><?php echo $j; ?></span></a>

			  			<?php $j++; ?>

			        <?php endwhile;?>

			    

	        </nav>

	    	<?php wp_reset_query(); ?>



	 		

	    	



	    	<div id="container-0" class="container theme-1 box-form">

				<header class="intro">

					

					<div class="intro__content">

						<form id="form" data-parsley-validate="">

							<span class="falta">Falta<br> <strong>

							<?php

							$datetime1 = new DateTime("now");

							$datetime2 = date_create('2016-04-10');

							$interval = date_diff($datetime1, $datetime2);

							echo $interval->format('%a%'); ?>

                            </strong><br> dias</span>

							<h2>Antes de empezar</h2>

							<div class="group-control">

								<label>¿De donde nos visitas?</label>

								<select id="departamento" name="departamento" required>


								</select>

								<select id="provincia" name="provincia" required>

									<option  value=''>[Seleccione Provincia]</option>

								</select>

								<select id="distrito" name="distrito" required>

									<option  value=''>[Seleccione Distrito]</option>

								</select>

							</div>

							<div class="group-control genero">

								<label class="lbl-left">¿Cuál es tu género?</label>

								<input type="radio" name="genero" id="masculino" class="masculino" checked="checked" value="M" required />

								<label for="masculino">Masculino</label>

								<input type="radio" name="genero" id="femenino" class="femenino" value="F" required />

								<label for="femenino">Femenino</label>

							</div>

							<div class="group-control two-col">

								<label>¿Qué edad tienes?</label>

								<input type="text" placeholder="Ingresa tu edad" name="edad" required data-parsley-type="digits" maxlength="3" data-parsley-min="15" data-parsley-max="100" />

							</div>

							<div class="group-control two-col">

								<label>Grado de instrucción</label>

								<select name="grado" id="grado" required >

									<option  value=''>[Seleccione Grado]</option>

									<option value="universitaria_completa">Universitaria completa</option>

									<option value="universitaria_incompleta">Universitaria incompleta</option>

									<option value="tecnico_incompleto">Técnico completo</option>

									<option value="tecnico_incompleto">Técnico incompleto</option>

									<option value="secundaria_completa">Secundaria completa</option>

									<option value="secundaria_incompleta">Secundaria incompleta</option>

									<option value="primaria_completa">Primaria completa</option>

									<option value="primaria_incompleta">Primaria incompleta</option>

								</select>

							</div>

							

							<div>

								<p><a class="thumb-nav__item omitir" onclick="skipUbigeo()" href="#">Saltar formulario &#187;</a></p>

							</div>



							<div class="group-control">

								<button type="submit" onclick="setUserData()">¡Empezar!</button>

							</div>

						</form>

					</div><!-- /intro__content -->

				</header><!-- /intro -->

				

			</div><!-- /container -->



			

			<?php $i = 1; ?>

			<?php 

	            $cant_preguntas = query_posts(array(

	                'post_type' => 'preguntas',

	                'showposts' => 50,

	                'orderby' => 'menu_order',

	                'order'   => 'DESC'

	            ) );  

	            $cant_preguntas = count($cant_preguntas);
	        ?>

	        

		<?php while (have_posts()) : the_post(); ?>

			<div id="container-<?php echo $i; ?>" class="container theme-1">

				<header class="intro">

					

					<div class="intro__content">

						<h1><?php the_title(); ?></h1>

						<h3>¿Qué tan importante es para usted esta pregunta?</h3>

							

						<p class="importancia">

							<a href="#" class="baja" onclick="saveImportanceForQuestion(<?php echo $i - 1; ?>,0,this)">1</a>

							<a href="#" class="media active" onclick="saveImportanceForQuestion(<?php echo $i - 1; ?>,1,this)">2</a>

							<a href="#" class="alta" onclick="saveImportanceForQuestion(<?php echo $i - 1; ?>,2,this)">3</a>

						</p>

						

						<?php if($i < $cant_preguntas){ ?>

						

						<button class="btn btn-1 btn-1a" data-index="<?php echo $i; ?>" onclick="saveOpinionForQuestion(<?php echo $i; ?>,'de_acuerdo', <?php echo the_ID();?> )">DE ACUERDO</button>

						<button class="btn btn-1 btn-1a" data-index="<?php echo $i; ?>" onclick="saveOpinionForQuestion(<?php echo $i; ?>,'neutral',<?php echo the_ID();?>)">NEUTRAL</button>

						<button class="btn btn-1 btn-1a" data-index="<?php echo $i; ?>" onclick="saveOpinionForQuestion(<?php echo $i; ?>,'desacuerdo', <?php echo the_ID();?>)">EN DESACUERDO</button>

						

						<?php }else{ ?>

						
						<!--onsubmit="finish(this)"-->
						<form action = "<?php echo site_url(); ?>/resultado" method="POST"  class="last-quest">

						<button class="btn btn-1 btn-1a" type="button" onclick="saveOpinionForQuestion(<?php echo $i ; ?>,'de_acuerdo', <?php echo the_ID();?> )">DE ACUERDO</button>

						<button class="btn btn-1 btn-1a" type="button" onclick="saveOpinionForQuestion(<?php echo $i ; ?>,'neutral',<?php echo the_ID();?>)">NEUTRAL</button>

						<button class="btn btn-1 btn-1a" type="button" onclick="saveOpinionForQuestion(<?php echo $i ; ?>,'desacuerdo', <?php echo the_ID();?>)">EN DESACUERDO</button>

						<input type="hidden" name="userQuestionResults" value="" id="userQuestionResults">
						<input type="hidden" name="userData" value="" id="userData">

						</form>				

						<?php } ?>

						

						<p><a class="thumb-nav__item omitir" onclick="$('#preg-<?php echo ($i+1); ?> > span').trigger('click');" href="#">Omitir &#187;</a></p>

					</div><!-- /intro__content -->

					

				</header><!-- /intro -->

				

			</div><!-- /container -->
			

			<?php $i++; ?>

		<?php endwhile;?>

		

		<?php wp_reset_query(); ?>



		<?php $i = 1; ?>

		<?php 

            $cant_preguntas = query_posts(array(

                'post_type' => 'preguntas',

                'showposts' => 50,

                'orderby' => 'menu_order',

                'order'   => 'ASC'

            ) );  

            $cant_preguntas = count($cant_preguntas);
        ?>

	        
        <div class="box-rpts box-resumen" style="display:none;">

			<?php while (have_posts()) : the_post(); ?>
				
				<div id="rpts-<?php echo $i; ?>" class="box Wrapper ">
					<?php 
			        	$id_pregunta = get_the_id();
			        	global $wpdb;
						$results = $wpdb->get_results( 'SELECT id_candidato, preguntas, links, comentarios FROM candidato_preguntas', OBJECT );
			         	shuffle($results);
			         	foreach ($results as $key => $value) {

			         		$comentarios = json_decode($value->comentarios, true);
			         		$link_youtube = json_decode($value->links, true);
			         		$txt ="";
			         		$video ="";
			         		if ( $comentarios != null ) {
			         			$txt = array_key_exists( $id_pregunta , $comentarios ) ? $comentarios[$id_pregunta]:"";
			         		}
			         		if ( $link_youtube != null ) {
			         			$video = array_key_exists( $id_pregunta , $link_youtube ) ? $link_youtube[$id_pregunta]:"";
			         		}

			         		$foto_candidato = get_field('foto_perfil', $value->id_candidato ); 
		         			$nombre_candidato =get_post( $value->id_candidato )->post_title;
		         			$partido_candidato =get_field('partido', $value->id_candidato );
				         	
				         	if ( trim($txt) != "" || trim($video) != "" ) {
				         		
				         		//Imprimir los Candidatos que respondieron
				         		?>

				         			<section class="resumen-respuestas <?php echo $respuestas[$id_pregunta]; ?>">
				         				
				         				<aside>
				         					<img src="<?php echo $foto_candidato; ?>?v=2">
				         					<h4><?php echo $nombre_candidato; ?></h4>
				         					<h5><?php echo $partido_candidato; ?></h5>
				         				</aside>
				         				<article>
				         					<?php echo $txt; ?>
				         				</article>
				         				<?php 
				         					if ($video!= "") {
				         						$parts = parse_url($video);
												parse_str($parts['query'], $query);
												$v = $query['v']; ?>
				         					<a href="http://www.youtube.com/embed/<?php echo $v; ?>?autoplay=1" class="ver-video fancybox.iframe"><span class="icon-video"></span> ver video</a>	
										<?php
				         					}
					         					

				         				 ?>
				         				

				         			</section>


				         		<?php
				         	}
				         	
			         	}

			          ?>
			    </div> 
				

				<?php $i++; ?>

			<?php endwhile;?>
		</div>



		<script src="<?php echo get_template_directory_uri(); ?>/js/classie.js"></script>

		<script>

			

			

			

		</script>
	</div>
</div>

	<div class="view-rpts" style="display:none;">
		<div class="Wrapper resumen">
				<h2><span>Respuestas</span></h2>
			</div>
		<div id="respuestas-candidatos">
			
		</div>
	</div>
	<footer>

		<div class="Wrapper">
			GPS electoral - IDEA Internacional 2016
			
		</div>
	</footer>
</div>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js?v=13"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/vendor/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<?php wp_footer(); ?>

</body>
</html>
