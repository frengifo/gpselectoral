<?php get_header(); ?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/noticias.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.news.css">

<div id="main" class="Wrapper content-noticias">
   
   <div class="row Wrapper ">
      <div class="col-md-8 nopadding noticias-home ">
         <section class="ultimas-noticias ">
            <div class="row">
            <?php while ( have_posts() ) : the_post(); ?>
               <article class="col-md-12 col-sm-12 col-xs-12">
                  
                  
                  <h3><a href="<?php the_permalink(); ?>"><?php the_title( ); ?></a></h3>
                  <p class="fecha-noticia"><small><?php the_date(); ?> <!--2 de Marzo del 2016--></small></p>
                  <p><?php the_excerpt(); ?></p>
                  <a href="<?php the_permalink(); ?>">
                    <img src="http://gpselectoral.pe/wp-content/uploads/2016/03/000300_99515.jpg" alt="000300_99515" width="100%" class="alignnone size-medium wp-image-315" srcset="http://gpselectoral.pe/wp-content/uploads/2016/03/000300_99515-300x169.jpg 300w, http://gpselectoral.pe/wp-content/uploads/2016/03/000300_99515.jpg 604w" sizes="(max-width: 604px) 100vw, 604px">
                  </a>

               </article>
            <?php endwhile; ?> 
            </div>
         </section>
      </div>
      <div class="col-md-4 nopadding aside-left">
        <div class="fb-page" data-href="https://www.facebook.com/gpselectoral" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
             <div class="fb-xfbml-parse-ignore">
                <blockquote cite="https://www.facebook.com/gpselectoral"><a href="https://www.facebook.com/gpselectoral">GPS Electoral</a></blockquote>
             </div>
          </div>
      </div>
   </div>

</div>


<?php get_footer(); ?>