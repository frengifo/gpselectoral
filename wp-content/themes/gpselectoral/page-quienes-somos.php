      
<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/quienes-somos.css?v=4" media="all">
         <div id="wrapper" class="">
            <div id="sliders-container"></div>
            <div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
               <div class="fusion-page-title-row">
                  <div class="fusion-page-title-wrapper">
                     <div class="fusion-page-title-captions">
                        <h1 class="entry-title" >Quiénes Somos</h1>
                     </div>
                     <div class="fusion-page-title-secondary">
                        <div class="fusion-breadcrumbs"><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="<?php echo site_url(); ?>"><span itemprop="title">Inicio</span></a></span><span class="fusion-breadcrumb-sep">/</span><span class="breadcrumb-leaf">Quiénes Somos</span></div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="main" class="clearfix Wrapper" style="">
               <div class="fusion-row" style="">
                  <div id="content" style="width: 100%;">
                     <div id="post-2776" class="post-2776 page type-page status-publish hentry">
                        <span class="entry-title" style="display: none;">Quienes Somos</span><span class="vcard" style="display: none;"><span class="fn"><a href="http://theme-fusion.com/avada/author/admin/" title="Posts by admin" rel="author">admin</a></span></span>
                        <div class="post-content">
                           <div class="fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;">
                              <div class="fusion-column-wrapper">
                                 
                                 <p>GPS Electoral es la iniciativa de  instituciones con prestigio global en la promoción de la democracia: el  Instituto Holandés para la Democracia Multipartidaria (NIMD, por sus siglas en  inglés) y el Instituto Internacional para la Democracia y Asistencia Electoral  (IDEA) y que cuenta con el apoyo del Grupo RPP.</p>
                                 <div class="fusion-clearfix"></div>
                              </div>
                              <div class="fusion-title title fusion-title-size-two">
                                 <h2 class="title-heading-left" data-fontsize="18" data-lineheight="27">IDEA Internacional </h2>
                                 <div class="title-sep-container">
                                    <div class="title-sep sep-double"></div>
                                 </div>
                              </div>
                              <p><img src="http://gpselectoral.pe/wp-content/uploads/2016/03/idea_logo.png" hspace="10" align="left" />IDEA es una organización  intergubernamental fundada en 2004 con el mandato de brindar apoyo para lograr  democracias más sostenibles, efectivas y legítimas. IDEA es Observador  Permanente ante las Naciones Unidas.<br>
                                 Perú es uno de los 28  países que forman parte de IDEA desde 2004.<br>
                                 IDEA genera conocimiento comparado sobre elecciones,  constituciones, partidos políticos y desarrollo, así como la relación entre la  democracia y el género, la diversidad, los conflictos y la seguridad. IDEA pone  este conocimiento al servicio de actores nacionales que trabajan en reformas  democráticas, y facilita el diálogo necesario para lograr el cambio  democrático.<br>
                                 IDEA actúa en todo el mundo. El Instituto  tiene su sede en Estocolmo, Suecia, y cuenta con oficinas en las regiones de  África, América Latina y el Caribe, Asia y el Pacífico, Asia Occidental y el  Norte de África.
                              </p>
                              <div class="fusion-clearfix"></div>
                              <div class="fusion-title title fusion-title-size-two">
                                 <h2 class="title-heading-left" data-fontsize="18" data-lineheight="27">NIMD, Instituto Holandés para la Democracia Multipartidaria</h2>
                                 <div class="title-sep-container">
                                    <div class="title-sep sep-double"></div>
                                 </div>
                              </div>
                              <p><img src="http://gpselectoral.pe/wp-content/uploads/2016/03/nimd_logo.png" hspace="10" align="left" />NIMD fue fundada en el año 2000 por siete  partidos políticos de Holanda con el objetivo de dar asistencia a partidos  políticos en democracias nuevas o en consolidarse.<br>
                                 NIMD ha apoyado más de 150 partidos  políticos y movimientos democráticos en más de 25 países de África, América  Latina, el Medio Oriente y el Cáucaso del Sur. <br>
                                 NIMD se involucra directamente con  partidos políticos para fortalecer su organización y la construcción de  consensos en agendas de reforma. Su composición multipartidaria le facilita trabajar  de modo imparcial con líderes de todo el espectro político, particularmente en  procesos de reforma políticamente sensitivos. 
                              </p>
                             <div class="fusion-clearfix"></div>
                              <div class="fusion-title title fusion-title-size-two">
                                 <h2 class="title-heading-left" data-fontsize="18" data-lineheight="27">RPP Noticias</h2>
                                 <div class="title-sep-container">
                                   <div class="title-sep sep-double"></div>
                                 </div>
                              </div>
                              <p><img src="http://gpselectoral.pe/wp-content/uploads/2016/03/rpp_logo.png" hspace="10" align="left" />Desde hace más de 52 años, RPP Noticias se ha consolidado como el medio de mayor credibilidad del país, uniendo y llevando información a todos los peruanos, marcando la agenda noticiosa gracias al despliegue de nuestros periodistas ubicados en todos los rincones del mundo, que se movilizan cada instante para ofrecer las noticias en el mismo momento que ocurren a lo largo de todo el Perú y el extranjero.</p>
                              <p>Luego de este largo tiempo de emisiones ininterrumpidas de noticias, servicios y entretenimiento, utilizando tecnología de vanguardia, continuamos integrando al Perú porque somos el único medio de comunicación que nunca se detiene. Nuestra constante evolución nos permite informar de manera inmediata, objetiva y veraz, durante las 24 horas del día, a nuestros más de seis millones de oyentes semanales y a todos los usuarios de nuestra multiplataforma de comunicación (radio, televisión y web), lo que convierte a RPP Noticias en una potente e íntegra red informativa que transmite el espíritu de un país que avanza.</p>
<div class="fusion-clearfix"></div>
                           </div>
                        </div>
                        
                        <div class="fusion-separator fusion-full-width-sep sep-none"></div>
                        <section id="clientes"></section>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <footer>
           <div class="Wrapper">
              GPS electoral - IDEA Internacional 2016
           </div>
        </footer>
      </div>
      
      </div>
      <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/main.js?v=13"></script>
   </body>
</html>
