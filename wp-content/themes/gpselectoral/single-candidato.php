<!DOCTYPE html>
<html class="pagInterna" lang="es">
	<head>
		<meta charset="utf-8">
		<title><?php echo $post->post_title; ?></title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width">
		<link rel="stylesheet" href="<?=get_template_directory_uri(); ?>/css/main.css">
		<script src="<?=get_template_directory_uri(); ?>/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		
		<!-- Fav and touch icons -->
	</head>
	
	<body style="height:100%;">
		
		<div id="loader"></div>
		<div id="wrapperAjax" style="height:100%;">
			<!--Nav Bar -->
			<div class="shown logoMiniInterna">
				<div class="detalhe"></div>
			</div>
			<!--Home Section -->
			<section id="home" class="pagInterna" style="height:100%;">
				<div class="bx-wrapper" style="height:100%;">
					<div class="session-gallery" id="session-gallery">
					</div>
				</div>
			</section>
			<div class="controllers">
				<div class="voltarInt animado"><div class="before">Cerrar</div></div>
				<a href="projetos/guarida-imoveis" data-titulo="Guarida Imóveis | Plano Transmedia" data-url="trabajos/" class="prevP animado"><div class="before">Próximo Trabajo</div></a>
				<a href="projetos/iplace" data-titulo="iPlace | Plano Transmedia" data-url="trabajos/" class="nextP animado"><div class="before">Trabajo Anterior</div></a>
			</div>
			
		</div>
		<!-- Javascript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script>
			window.jQuery || document.write('<script src="<?=get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js"><\/script>')
		</script>
		<script src="<?=get_template_directory_uri(); ?>/js/plugins.js"></script>
		<script src="<?=get_template_directory_uri(); ?>/js/main.js"></script>
	</body>
</html>