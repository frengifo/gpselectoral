      
<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/quienes-somos.css?v=4" media="all">
         <div id="wrapper" class="">
            <div id="sliders-container"></div>
            <div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
               <div class="fusion-page-title-row">
                  <div class="fusion-page-title-wrapper">
                     <div class="fusion-page-title-captions">
                        <h1 class="entry-title" >Ponte en contacto con nosotros</h1>
                     </div>
                     <div class="fusion-page-title-secondary">
                        <div class="fusion-breadcrumbs"><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="<?php echo site_url(); ?>"><span itemprop="title">Inicio</span></a></span><span class="fusion-breadcrumb-sep">/</span><span class="breadcrumb-leaf">Contacto</span></div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="main" class="clearfix Wrapper" style="">
               <div class="fusion-row" style="">
                  <div id="content" style="width: 100%;">
                     <div id="post-2776" class="post-2776 page type-page status-publish hentry">
                        <span class="entry-title" style="display: none;">Contacto</span><span class="vcard" style="display: none;"><span class="fn"><a href="Contacto" title="Posts by admin" rel="author">admin</a></span></span>
                        <div class="post-content">
                        <div class="fusion-title title fusion-title-size-two" style="margin-bottom:0px;">
                        <!--
                        <h2 class="title-heading-left" data-fontsize="18" data-lineheight="27">Ponte en contacto con nosotros</h2>
                        -->
						<div class="title-sep-container">
                        <!-- <div class="title-sep sep-double"></div> -->
                        </div></div>
						<div style="width: 70%;">Envíanos un mensaje, en los casilleros registre la consulta a realizar, y te daremos una respuesta.</div>
						<p><?php echo do_shortcode( '[contact-form-7 id="299" title="Contactanos"]' ); ?></p>
                        </div>
                        
                       
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <footer>
           <div class="Wrapper">
              GPS electoral - IDEA Internacional 2016
           </div>
        </footer>
      </div>
      
      </div>
      <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/main.js?v=13"></script>
   </body>
</html>
