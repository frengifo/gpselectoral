      
<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/quienes-somos.css?v=4" media="all">
         <div id="wrapper" class="">
            <div id="sliders-container"></div>
            <div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
               <div class="fusion-page-title-row">
                  <div class="fusion-page-title-wrapper">
                     <div class="fusion-page-title-captions">
                        <h1 class="entry-title" >Entrevista a candidat@s presidenciales 2016 </h1>
                    </div>
                     <div class="fusion-page-title-secondary">
                        <div class="fusion-breadcrumbs"><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="<?php echo site_url(); ?>"><span itemprop="title">Inicio</span></a></span><span class="fusion-breadcrumb-sep">/</span><span class="breadcrumb-leaf">Entrevistas</span></div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="main" class="clearfix Wrapper" style="">
               <div class="fusion-row" style="">
                  <div id="content" style="width: 100%;">
                     <div id="post-2776" class="post-2776 page type-page status-publish hentry">
                        <span class="entry-title" style="display: none;">Preguntas Frecuentes</span><span class="vcard" style="display: none;"><span class="fn"><a href="Preguntas Frecuentes" title="Posts by admin" rel="author">admin</a></span></span>
                        <div class="post-content">
                           <div class="fusion-two-third fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;">
                              <p>Entrevista Ántero Flores Aráoz </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/SL7hbEW2I-g" frameborder="0" allowfullscreen></iframe>
                              
                              <br><br>
                              <p>Entrevista Alejandro Toledo </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/ryvlbuNBgDU" frameborder="0" allowfullscreen></iframe>
                              
                              <br><br>
                              <p>Entrevista Yehude Simon </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/x9F45QXWeRE" frameborder="0" allowfullscreen></iframe>
                              
                              <br><br>
                              <p>Entrevista Veronika Mendoza </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/EJ8-RiHJ6Jo" frameborder="0" allowfullscreen></iframe>  
                              
                              <br><br>
                              <p>Entrevista Vladimir Cerrón</p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/ffbNyl4XSCM" frameborder="0" allowfullscreen></iframe>
                              
                              <br><br>
                              <p>Entrevista Pedro Pablo Kuczynski </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/9s3rRvdsOCY" frameborder="0" allowfullscreen></iframe>                                                                                                                                                   
                              <br><br>
                              <p>Entrevista Hernando Guerra García  </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/AEcSgpNI6_Q" frameborder="0" allowfullscreen></iframe>    
                              
                              <br><br>
                              <p>Entrevista Francisco Diez Canseco  </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/xOxumPB-5cE" frameborder="0" allowfullscreen></iframe>
                              
                              <br><br>
                              <p>Entrevista Alan García  </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/Lz0E7UGUJ8M" frameborder="0" allowfullscreen></iframe>

                              <br><br>
                              <p>Entrevista Alfredo Barnechea   </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/ymGLuntRMRg" frameborder="0" allowfullscreen></iframe>
                              
                              <br><br>
                              <p>Entrevista Fernando Olivera   </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/fvORgxCPb7w" frameborder="0" allowfullscreen></iframe>
                              
                              <br><br>
                              <p>Entrevista Miguel Hilario   </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/1MeMcP_6dIA" frameborder="0" allowfullscreen></iframe>                                                                                                                                                                   
                              
                              <div class="fusion-clearfix"></div>
                           </div>
                        </div>
                        <div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-yes">
                           <div class="fusion-column-wrapper">
                              
                              <div class="fb-page" data-href="https://www.facebook.com/gpselectoral" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                 <div class="fb-xfbml-parse-ignore">
                                    <blockquote cite="https://www.facebook.com/gpselectoral"><a href="https://www.facebook.com/gpselectoral">GPS Electoral</a></blockquote>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-separator fusion-full-width-sep sep-none"></div>
                                                
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <footer>
           <div class="Wrapper">
              GPS electoral - IDEA Internacional 2016
           </div>
        </footer>
      </div>
      
      </div>
      <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/main.js?v=13"></script>
   </body>
</html>
