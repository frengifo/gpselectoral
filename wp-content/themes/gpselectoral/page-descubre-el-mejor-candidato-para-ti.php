    

        <?php /**********************HEADER***************************/ ?>

            <?php get_header(); ?>

        <?php /**********************ENDHEADER***************************/ ?>

        <script src ="<?php echo get_template_directory_uri(); ?>/js/ubigeo.js"></script>

        <?php while ( have_posts() ) : the_post(); ?>



                <?php /**********************HOME***************************/ ?>
                <!--Home Section -->
                <section id="home" class="bannerHome">
                    <div id="bannerHome" class="bannerWrapper">
                        <div class="bannerPag">
                            <div class="viraEsq"></div>
                            <div class="viraDir"></div>
                        </div>
                        <div class="slidesWrapper">

                            <?php  $gallery = get_post_gallery( get_the_ID(), false ); ?>
                            <?php $ids = explode(",", $gallery['ids']); ?>
                            <?php $i=1; ?>
                            <?php foreach( $gallery['src'] as $src ) : ?>

                                    <?php $full = explode("-150x150.", $src)[0].".".explode("-150x150.", $src)[1]; ?>

                                    <div data-pos="<?php echo $i; ?>" class="slide <?php echo $i == 1 ? "atual":""; ?> branco">
                                        <div class="imgBgBanner" data-src="<?php echo $full; ?>" data-alt="Banner Guarida"></div>
                                        <div class="vamo3 branco">

                                            <span class="slide-title"><?php echo get_post($ids[ $i - 1 ])->post_excerpt; ?> </span>
                                        </div>
                                        <a href="/test/" class="vamo4 branco">
                                            <span>Realiza el Test</span>
                                        </a>
                                    </div>

                                    
                                    <?php $i++; ?>
                            <?php endforeach ?>
                        </div>

                    </div>
                    <div class="pagHolderHome">
                        <div data-pos="1" class="pagItem pagItemAtivo"></div>
                    </div>
                </section>
                        
                <section id="links">
                	<nav>
                		<ul>
                			<li>
                				<a href="/quienes-somos">¿Quiénes somos?</a>
                			</li>
                			<li>
                				<a href="/preguntas-frecuentes">Preguntas frecuentes</a>
                			</li>
                			<li>
                				<a href="/contacto">Contáctenos</a>
                			</li>
                		</ul>
                	</nav>
                </section>

				<script type="text/javascript">

                    function postToFeed(picture, title, summary){
                        var obj = {method: 'feed',link: '<?php echo site_url()."/test/"; ?>', picture: picture,name: title, description: summary};
                        function callback(response){}
                        FB.ui(obj, callback);
                    }

                </script>
                <!--projetos Section -->
                <div class="remodal" data-remodal-id="candidato" data-remodal-options="hashTracking: false, closeOnOutsideClick: false">

                  <button data-remodal-action="close" class="remodal-close"></button>
                  <div class="single-content">
                      
                  </div>
                </div>
                <div style="background-color:#FFF" class="Wrapper">
                <p align="center" style="   color: #333;  margin: 0 5%;  margin-top: 2em;    font-size: .85em;">Todas las respuestas han sido obtenidas mediante entrevistas exclusivas a los candidatos y candidatas de cada partido, a excepción del partido Democracia Directa de quienes no obtuvimos respuesta alguna y el caso del partido Fuerza Popular donde las respuesta han sido sacadas del Plan de Gobierno presentado ante el JNE y por declaraciones en medios de comunicación.</p>
                </div>
              
                <section id="projetos">

                    <div class="fullWidth">

                        <ul class="cbp-rfgrid projetosGrid" id="cnd">

                        <?php 
                            query_posts(array( 
                                'post_type' => 'candidatos',
                                'showposts' => 25,
                                'orderby' => 'rand'
                            ) ); 
                            $j=0;
                        ?>
                        <?php while (have_posts()) : the_post(); ?>

                                <li class="candidato">

                                    <a href="#candidato-<?php echo $j; ?>" class="various" rel="gallery1" data-titulo="<?php the_title(); ?>" data-ordem="1">

                                        <img alt="Ubica un candidato a tu medida" src="<?php echo get_field("foto_perfil"); ?>?v=2" class="foto" />

										<figure class="foto-partido"><img src="<?php echo get_field("foto_partido"); ?>?v=2"></figure>
					
                                        <div class="clienteInfo">

                                            <span class="details nombre"> <?php
                                            $count_str = substr_count(get_the_title(), " ");
                                            
                                            $title = $count_str < 2 ? str_replace(" ", "<br>", get_the_title()): get_the_title();
                                            
                                            echo $title; ?> </span>

                                            <span class="bar btn-vermas"> <span>Ver más</span> </span>

                                            <h2 class="partido"><?php echo get_field("partido"); ?></h2>

                                        </div>

                                        <div class="littleProjectBar"></div>

                                    </a>

                                    <div class="info-candidato" id="candidato-<?php echo $j; ?>" style="display:none;">
                                        <div class="row">
                                                
                                            <div class="picture">
                                                <img src="<?php echo get_field("foto_perfil"); ?>?v=2">
                                                <section>
                                                    <img src="<?php echo get_field("foto_partido"); ?>?v=2" alt="Ubica un candidato a tu medida" class="pull-left">
                                                    <p><?php echo get_field("partido"); ?></p>
                                                </section>
                                            </div>
                                            <div class="text-info">
                                                <h2><?php the_title(); ?></h2>
                                                <?php the_content(); ?>
                                                <a href="<?php echo get_field("plan_de_gobierno"); ?>" target="_blank" class="download">Descargar plan de gobierno</a>
                                            </div>
                                            <section class="redes">
                                                <span><i class="fb"><a href="<?php echo get_field("link_facebook"); ?>" title="Facebook"><img src="<?php echo get_template_directory_uri(); ?>/img/fb.png?v=2" alt="Ubica un candidato a tu medida" /> </a> </i> <i class="tw"><a href="<?php echo get_field("link_twitter"); ?>" title="Twitter"><img src="<?php echo get_template_directory_uri(); ?>/img/tw.png?v=2" alt="Ubica un candidato a tu medida" /></a></i></span>
                                            </section>
                                        </div>

                                    </div>
                                </li>
                                <?php $j++; ?>
                        <?php endwhile;?>

                        <?php wp_reset_query(); ?>

                            

                            

                        </ul>

                    </div>

                    <div class="maisLoad animado"><div class="mais animado">+</div></div>

                    <div class="carregarMais animado">Cargar más</div>

                </section>

                <section id="about"></section>

                <!--<section id="clientes">


                    <div class="Wrapper">

                        <h5 style="text-align:center;">NOS RESPALDAN</h5>

                        <ul class="cbp-rfgrid">
                        	<?php 
	                            query_posts(array( 
	                                'post_type' => 'colaboradores',
	                                'showposts' => 25,
	                                'orderby' => 'rand'
	                            ) );  
	                        ?>
	                        <?php while (have_posts()) : the_post(); ?>

                            <li>
                            	<a href="<?php echo get_field("link"); ?>" target="_blank">
                            		<img class="" alt="Ubica un candidato a tu medida" src="<?php echo get_field("imagen"); ?>" />
                            	</a>
                            </li>

                            <?php endwhile;?>

                        	<?php wp_reset_query(); ?>
                        </ul>

                    </div>

                </section>-->


                <?php /***********************ENDHOME**************************/ ?>



        <?php endwhile; ?> 



        <?php /**********************FOOTER***************************/ ?>



            <?php get_footer(); ?>



        <?php /**********************ENDFOOTER***************************/ ?>