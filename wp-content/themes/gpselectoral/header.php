<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">

<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">

  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?php the_title(); ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <link href="<?php echo get_template_directory_uri(); ?>/img/logo_72.png?v=2" rel="apple-touch-icon-precomposed" sizes="72x72" >

  <link href="<?php echo get_template_directory_uri(); ?>/img/logo_114.png?v=2" rel="apple-touch-icon-precomposed" sizes="114x114" >

  <link href="<?php echo get_template_directory_uri(); ?>/img/logo_196.png?v=2" rel="shortcut icon" sizes="196x196" >

    <!-- Styles -->

    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png?v=2" type="image/x-icon">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css?v=2">

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:100,300,400' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css?v=2">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/layout-multi.css?v=11">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css?v=13">

    <link rel="stylesheet" media="(max-width: 500px)" href="<?php echo get_template_directory_uri(); ?>/css/loader.css?v=11" />

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/vendor/source/jquery.fancybox.css?v=33" type="text/css" media="screen" />

    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Ubica un candidat@ a tu medida" />
    <meta property="og:description" content="El GPS Electoral es un test en línea que compara tus posiciones sobre temas de interés nacional con las posiciones de los partidos y postulantes a la Presidencia de la República." />
    <meta property="og:url" content="http://gpselectoral.pe/" />
    <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/img/share-gps.jpg" />
    <meta property="og:site_name" content="GPS Electoral" />
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description" content="El GPS Electoral es un test en línea que compara tus posiciones sobre temas de interés nacional con las posiciones de los partidos y postulantes a la Presidencia de la República."/>
    <meta name="twitter:title" content="GPS Electoral"/>

    <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.6.2-respond-1.1.0.min.js?v=2"></script>
	
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54746521-2', 'auto');
  ga('send', 'pageview');

</script>

        <!-- Fav and touch icons -->

	<!--[if lt IE 9]>

	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>

	<![endif]-->

	<?php //wp_head(); ?>

  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1199839313379586',
        xfbml      : true,
        version    : 'v2.5'
      });
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  </script>

</head>



<body <?php body_class(); ?>>

    <div class="sb-slidebar sb-right menu-right">
      <ul>
          <li>
              <a href="http://gpselectoral.pe/#cnd">Candidat@s</a>
          </li>
          <li>
              <a href="">Noticias</a>
          </li>
          <li>
              <a href="#" target="_blank">Videos</a>
              <ul class="dropdown">
              					<li><a href="http://gpselectoral.pe/videos-gps-electoral">Videos GPS Electoral</a></li>
                				<li><a href="http://gpselectoral.pe/entrevista-candidatos">Entrevista a candidat@s presidenciales</a></li>
              </ul>
          </li>
          <li>
              <a href="http://gpselectoral.pe/test/" target="_self">Test</a>
          </li>
      </ul>
    </div>
    <header>
        <section class="Wrapper">
          <div class="logos-box">
             <a href="<?php echo site_url(); ?>" class="pull-left"><img src="http://gpselectoral.pe/wp-content/uploads/2016/01/logo_min.png?v=2" alt="Ubica un candidato a tu medida"  class="logo"></a>

             

             <a href="http://rpp.pe/" target="_blank" class="pull-right"><img src="http://gpselectoral.pe/wp-content/uploads/2016/03/rpp_plano.png?v=2" alt="Ubica un candidato a tu medida" hspace="10" class="logo-rpp"></a>
             <a href="http://www.idea.int/es/" target="_blank" class="pull-right"><img src="http://gpselectoral.pe/wp-content/uploads/2016/03/idea_plano.png?v=2" alt="Ubica un candidato a tu medida" class="logo-idea"></a>
             <a href="http://nimd.org/" target="_blank" class="pull-right"><img src="http://gpselectoral.pe/wp-content/uploads/2016/03/logo_nimd_30.png?v=2" alt="Ubica un candidato a tu medida" class="logo-netherlands"></a>
          </div>
          <div id="nav-icon4" class="sb-toggle-right">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </section>
    </header>
    <section class="down-menu">
      <div class="Wrapper">
        <nav class="menu">
            <ul>
                <li>
                  <a href="http://gpselectoral.pe/#cnd">Candidat@s</a>
                </li>
                <li>
                  <a href="<?php echo site_url(); ?>/noticias">Noticias</a>                     
                </li>
                <li>
                  <a href="#" target="_blank">Videos</a>
                  
                      <ul class="dropdown">
                        <li><a href="http://gpselectoral.pe/videos-gps-electoral">Videos GPS Electoral</a></li>
                        <li><a href="http://gpselectoral.pe/entrevista-candidatos">Entrevista a candidat@s presidenciales</a></li>
                      </ul> 
                </li>
                <li>
                  <a href="/test/">Test</a>
                </li>
            </ul>
       </nav>
      </div>
    </section>
    <ul class='social'>
      <li>
        <a class="fa fa-facebook" href="https://www.facebook.com/gpselectoral/?fref=ts" target="_blank">    
          <span>Facebook</span>
        </a> 
      </li>
      <li>
        <a class="fa fa-twitter" href="https://twitter.com/gpselectoral"  target="_blank">
          <span>Twitter</span>
        </a>
      </li>
      <li>
        <a class="fa fa-share-alt" onclick="postToFeed('<?php echo get_template_directory_uri(); ?>/img/share-gps.jpg', 'Ubica un candidat@ a tu medida', 'El GPS Electoral es un test en línea que compara tus posiciones sobre temas de interés nacional con las posiciones de los partidos y postulantes a la Presidencia de la República.');" href="javascript:;">
        <span>Compartir</span>
        </a> 
      </li>
    </ul>
    <div id="sb-site">
