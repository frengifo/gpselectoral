      
<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/quienes-somos.css?v=4" media="all">
         <div id="wrapper" class="">
            <div id="sliders-container"></div>
            <div class="fusion-page-title-bar fusion-page-title-bar-breadcrumbs fusion-page-title-bar-left">
               <div class="fusion-page-title-row">
                  <div class="fusion-page-title-wrapper">
                     <div class="fusion-page-title-captions">
                        <h1 class="entry-title" >Videos GPS Electoral 2016</h1>
                    </div>
                     <div class="fusion-page-title-secondary">
                        <div class="fusion-breadcrumbs"><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="<?php echo site_url(); ?>"><span itemprop="title">Inicio</span></a></span><span class="fusion-breadcrumb-sep">/</span><span class="breadcrumb-leaf">Videos</span></div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="main" class="clearfix Wrapper" style="">
               <div class="fusion-row" style="">
                  <div id="content" style="width: 100%;">
                     <div id="post-2776" class="post-2776 page type-page status-publish hentry">
                        <span class="entry-title" style="display: none;">Preguntas Frecuentes</span><span class="vcard" style="display: none;"><span class="fn"><a href="Preguntas Frecuentes" title="Posts by admin" rel="author">admin</a></span></span>
                        <div class="post-content">
                           <div class="fusion-two-third fusion-layout-column fusion-spacing-yes" style="margin-top:0px;margin-bottom:20px;">
                              <p>Conoce a l@s 17 postulantes a la Presidencia del Perú </p>
                             <iframe width="560" height="315" src="https://www.youtube.com/embed/3Wc8fr3i4T0" frameborder="0" allowfullscreen></iframe>
                              <br><br>
                              <p>#ComentaSinRoche: ¿El Estado y el sector privado deben unirse para mejorar la salud y educación?  </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/yS67s3ZDpTE" frameborder="0" allowfullscreen></iframe>
                              
                              <br><br>
                              <p>#ComentaSinRoche: ¿Debe despenalizarse el aborto en caso de violación?  </p>
                              <iframe width="560" height="315" src="https://www.youtube.com/embed/jTGcgOvMUrQ" frameborder="0" allowfullscreen></iframe>
                              
                                                                                                                                                                                                 
                              
                              <div class="fusion-clearfix"></div>
                           </div>
                        </div>
                        <div class="fusion-one-third fusion-layout-column fusion-column-last fusion-spacing-yes">
                           <div class="fusion-column-wrapper">
                              
                              <div class="fb-page" data-href="https://www.facebook.com/gpselectoral" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                 <div class="fb-xfbml-parse-ignore">
                                    <blockquote cite="https://www.facebook.com/gpselectoral"><a href="https://www.facebook.com/gpselectoral">GPS Electoral</a></blockquote>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="fusion-separator fusion-full-width-sep sep-none"></div>
                                                
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <footer>
           <div class="Wrapper">
              GPS electoral - IDEA Internacional 2016
           </div>
        </footer>
      </div>
      
      </div>
      <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/main.js?v=13"></script>
   </body>
</html>
