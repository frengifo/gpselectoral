<!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">

<head>

  <meta charset="<?php bloginfo( 'charset' ); ?>">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?php the_title(); ?></title>

  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

  <link href="<?php echo get_template_directory_uri(); ?>/img/logo_72.png" rel="apple-touch-icon-precomposed" sizes="72x72" >

  <link href="<?php echo get_template_directory_uri(); ?>/img/logo_114.png" rel="apple-touch-icon-precomposed" sizes="114x114" >

  <link href="<?php echo get_template_directory_uri(); ?>/img/logo_196.png" rel="shortcut icon" sizes="196x196" >

    <!-- Styles -->

    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" type="image/x-icon">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:100,300,400' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/layout-multi.css?v=11">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css?v=13">

    <link rel="stylesheet" media="(max-width: 500px)" href="<?php echo get_template_directory_uri(); ?>/css/loader.css?v=11" />

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/vendor/source/jquery.fancybox.css?v=33" type="text/css" media="screen" />

    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Ubica un candidat@ a tu medida" />
    <meta property="og:description" content="El GPS Electoral es un test en línea que compara tus posiciones sobre temas de interés nacional con las posiciones de los partidos y postulantes a la Presidencia de la República." />
    <meta property="og:url" content="http://gpselectoral.pe/" />
    <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/img/share-gps.jpg" />
    <meta property="og:site_name" content="GPS Electoral" />
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:description" content="El GPS Electoral es un test en línea que compara tus posiciones sobre temas de interés nacional con las posiciones de los partidos y postulantes a la Presidencia de la República."/>
    <meta name="twitter:title" content="GPS Electoral"/>

    <script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54746521-2', 'auto');
  ga('send', 'pageview');

</script>

        <!-- Fav and touch icons -->

  <!--[if lt IE 9]>

  <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>

  <![endif]-->

  <?php //wp_head(); ?>

  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1199839313379586',
        xfbml      : true,
        version    : 'v2.5'
      });
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));
  </script>

</head>



<body <?php body_class(); ?>>

    <div class="sb-slidebar sb-right menu-right">
      <ul>
          <li>
              <a href="/quienes-somos">Quienes somos?</a>
          </li>
          <li>
              <a href="">Preguntas frecuentes</a>
          </li>
          <li>
              <a href="">Actvidades</a>
          </li>
          <li>
              <a href="https://www.youtube.com/channel/UCwSFsMYMGfn_DcQae_4FeQg" target="_blank">Videos</a>
          </li>
          <li>
              <a href="">Contactenos</a>
          </li>
      </ul>
    </div>
    <!--
    <header>
        <section class="Wrapper">
           <a href="<?php echo site_url(); ?>"><img src="http://gpselectoral.pe/wp-content/uploads/2016/01/logo_min.png" alt="Ubica un candidato a tu medida" class="logo"></a>

           <nav class="menu">
                    <ul>
                        <li>
                          <a href="">Quienes somos?</a>
                        </li>
                        <li>
                          <a href="">Preguntas frecuentes</a>
                        </li>
                        <li>
                          <a href="">Actvidades</a>
                        </li>
                        <li>
                          <a href="https://www.youtube.com/channel/UCwSFsMYMGfn_DcQae_4FeQg" target="_blank">Videos</a>
                        </li>
                        <li>
                          <a href="">Contactenos</a>
                        </li>
                    </ul>
           </nav>
            <div id="nav-icon4" class="sb-toggle-right">
              <span></span>
              <span></span>
              <span></span>
            </div>

        </section>
    </header>
    -->
    <ul class='social'>
      <li>
        <a class="fa fa-facebook" href="#">    
          <span>Facebook</span>
        </a> 
      </li>
      <li>
        <a class="fa fa-twitter" href="#">
          <span>Twitter</span>
        </a>
      </li>
      <li>
        <a class="fa fa-google-plus" href="#">
        <span>Google Plus</span>
        </a> 
      </li>
    </ul>
    <div id="sb-site">


		<?php while ( have_posts() ) : the_post(); ?>
<br><br>
			<?php //the_title(); ?>
			<?php the_content(); ?>

		<?php endwhile; ?> 

<!--
	<footer>

		<div class="Wrapper">
			GPS electoral - IDEA Internacional 2016
			
		</div>
	</footer>
-->	
</div>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.9.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js?v=13"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/vendor/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<?php wp_footer(); ?>

</body>
</html>