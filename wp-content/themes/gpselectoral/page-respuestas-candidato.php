<?php

if(!is_user_logged_in())
{
?>
<script>
	window.location = "http://gpselectoral.pe";
</script>
<?php
}

global $wpdb;

if(isset($_REQUEST) && isset($_REQUEST["candidateQuestions"] ))
{
	$questionsJsonString = stripslashes($_REQUEST["candidateQuestions"]);
	
	$candidateQuestionsJSON = json_decode($questionsJsonString , true);
	
	$linksJsonString = stripslashes($_REQUEST["candidateVideoLinks"]);
	
	$candidateLinksJSON = json_decode($linksJsonString , true);
	
	$commentsJsonString = stripslashes($_REQUEST["candidateComments"]);
	
	$candidateCommentsJSON = json_decode($commentsJsonString , true);
	
//	print_r($candidateLinksJSON);
	
	foreach($candidateQuestionsJSON as $candidateID => $questions)
	{		
		if($wpdb->get_var( 'SELECT COUNT(*) FROM candidato_preguntas where  id_candidato = '. $candidateID ))
		{
			//update
			$wpdb->update("candidato_preguntas",array("preguntas" => json_encode($questions),"links" => json_encode($candidateLinksJSON[$candidateID]), 
			"comentarios" => json_encode($candidateCommentsJSON[$candidateID])), array("id_candidato" => $candidateID));	
		}		
		else
		{
			//insert
			$wpdb->insert("candidato_preguntas", array("id_candidato" => $candidateID,"preguntas" => json_encode($questions),
			"links" => json_encode($candidateLinksJSON[$candidateID]), "comentarios" => json_encode($candidateCommentsJSON[$candidateID])));
		}
	}
	
	unset($_REQUEST);
}

?>

<?php //get_header(); ?>
	
	<style>
	
	#space{
	width : 100%;
	height : 50px;
	}
	
	.table_questions{
	
		display:none;
	}
	header{
		display: none !important;
	}
	.Wrapper{
		padding: 2em 0;
	}
	</style>

<div class="Wrapper">

	<div id="space">
	</div>
	
	<?php 
	    
	$questions = $wpdb->get_results( 'SELECT ID, post_title FROM wpor_posts where post_type = "preguntas" and post_status = "publish" order by ID DESC ', OBJECT ); 
	$questionsCount = $wpdb->get_var( 'SELECT COUNT(ID) FROM wpor_posts where  post_type = "preguntas"');
	
	$candidatesCount = 0;
	
	$candidates = $wpdb->get_results('SELECT post_title, ID FROM wpor_posts where  post_type = "candidatos" and post_status = "publish" order by ID DESC', OBJECT);
	
	?>
	
	<table width="100%">		
		<tr>
			<td> Candidat@ </td> 
			<td> 
				<select name="candidato" id="candidato" onchange = "selectCandidate(this)"> 
				
					<option> [Seleccione] </option>
				
					<?php foreach($candidates as $candidate): ?>		
					
					<option value = "<?php echo $candidate->ID; ?>"> <?php echo $candidate->post_title; ?> </option>
					
					<?php $candidatesCount++; ?>
					<?php endforeach; ?>
														
				</select> 
			</td>
			<td> <button onclick="finish()" id="guardar" class="btn btn-info pull-right btn-preguntas" style="display:none;"> Guardar </button> </td>
		</tr>

	</table>
		
	<?php $i = 1; ?>
		
	<?php foreach($candidates as $candidate): ?>	
	
	
	<table id="preguntas_<?php echo $i++ ;?>" border = "1" class = "table_questions">
		<tr>	
			<td colspan = "3"> <h3> Preguntas </h3> </td>
		</tr>
		
			<?php
			
			$candidateQuestions = $wpdb->get_results('SELECT * FROM candidato_preguntas where id_candidato = '.$candidate->ID, OBJECT );			
			
			if(isset($candidateQuestions[0]))
			{			
			
			$questionResults = json_decode($candidateQuestions[0]->preguntas);
			$linksResults = json_decode($candidateQuestions[0]->links);
			$commentsResults = json_decode($candidateQuestions[0]->comentarios);
			
			?>
			<?php $k=1; ?>
			<?php foreach($questions as $question): ?>		
			
			<tr>
				<td colspan = "3" style="text-align:center;">

				<h4> <?php echo $question->post_title ; ?> <span><?php echo $k; $k++;?></span></h4>
				<input type="hidden" value="<?php echo $question->ID; ?>" class = "question_id" />
				</td> 
			</tr>      				        
		        <tr class="options"> 		
			        <td> 
			        			        
			        <input type="radio" id="<?php echo $candidate->ID.'_'.$question->ID; ?>_acuerdo" name="opcion_<?php echo $candidate->ID.'_'.$question->ID; ?>"  
			        value = "de_acuerdo" class ="opcion_<?php echo $candidate->ID.'_'.$question->ID; ?>" 
			        onclick="saveQuestionsForSelectedCandidate()" 
			        <?php if(isset($questionResults->{$question->ID})&& $questionResults->{$question->ID} =="de_acuerdo"):?>
			        checked = "checked"
			        <?php endif; ?> /> 
	
			        <label for="<?php echo $candidate->ID.'_'.$question->ID; ?>_acuerdo">De acuerdo </label>
			        </td> 
			        
			        <td>
			         <input type="radio" id="<?php echo $candidate->ID.'_'.$question->ID; ?>_neutral" name="opcion_<?php echo $candidate->ID.'_'.$question->ID; ?>" 
			         value = "neutral" class ="opcion_<?php echo $candidate->ID.'_'.$question->ID; ?>" 
			         onclick="saveQuestionsForSelectedCandidate()"
			          <?php if(isset($questionResults->{$question->ID})&& $questionResults->{$question->ID} =="neutral"):?>
			        checked = "checked"
			        <?php endif; ?>/>
			        
			          <label for="<?php echo $candidate->ID.'_'.$question->ID; ?>_neutral">Neutral  </label>
			          </td> 		        
			        
			        <td> 
			        <input type="radio" id="<?php echo $candidate->ID.'_'.$question->ID; ?>_desacuerdo" name="opcion_<?php echo $candidate->ID.'_'.$question->ID; ?>"
			        value = "desacuerdo" class ="opcion_<?php echo $candidate->ID.'_'.$question->ID; ?>" 
			        onclick="saveQuestionsForSelectedCandidate()"  
			         <?php if(isset($questionResults->{$question->ID})&& $questionResults->{$question->ID} =="desacuerdo"):?>
			       checked = "checked" 
			        <?php endif; ?> />
			         <label for="<?php echo $candidate->ID.'_'.$question->ID; ?>_desacuerdo">Desacuerdo</label>
			         </td>
		        </tr>
		        <tr class="comments">
			        <td><small>Link de <strong>Youtube</strong></small></td>
			        <td colspan="2"><input type = "text" onkeyup="saveVideoLinksForSelectedCandidate()" class="link_video"
			         <?php if(isset($linksResults->{$question->ID})):?> 
				        value="<?php echo ($linksResults->{$question->ID}); ?>" 
			        <? endif;?> />
			        
			        </td>
		        </tr>		        
		        <tr class="comments">
			        <td><small>Comentario</small></td><td colspan="2"><textarea  class="comentario" onkeyup="saveCommentsForSelectedCandidate()" ><?php echo ($commentsResults->{$question->ID});?></textarea></td>
			</tr>
		        <tr> 
		        	<td colspan="3"> &nbsp </td> 
		        </tr>
		        <?php endforeach; ?>
		        
			<?php	
			}			
			else
			{
			?>
		
			<?php foreach($questions as $question): ?>		
			
			<tr>
				<td colspan = "3" style="text-align:center;">
				<b> <?php echo $question->post_title ; ?> </b>
				<input type="hidden" value="<?php echo $question->ID; ?>" class = "question_id"/>
				</td> 
			</tr>      				        
		        <tr> 		
			        <td> 
			        <input type="radio" name="opcion_<?php echo($candidate->ID.'_'.$question->ID); ?>"  
			        value = "de_acuerdo" class ="opcion_<?php echo($candidate->ID.'_'.$question->ID); ?>"  onclick="saveQuestionsForSelectedCandidate()" />
			        De acuerdo 
			        </td> 
			        <td>
			         <input type="radio" name="opcion_<?php echo($candidate->ID.'_'.$question->ID); ?>" 
			         value = "neutral" class ="opcion_<?php echo($candidate->ID.'_'.$question->ID); ?>" onclick="saveQuestionsForSelectedCandidate()"/>
			          Neutral  
			          </td> 		        
			        <td> 
			        <input type="radio" name="opcion_<?php echo($candidate->ID.'_'.$question->ID); ?>"  
			        value = "desacuerdo" class ="opcion_<?php echo($candidate->ID.'_'.$question->ID); ?>" onclick="saveQuestionsForSelectedCandidate()"/>
			         Desacuerdo
			         </td> 
		        </tr>	
		        <tr>
			        <td><b>Link de video</b></td><td colspan="2"><input type = "text" onkeyup="saveVideoLinksForSelectedCandidate()" class="link_video"/></td>
		        </tr>		        
		        <tr>
			        <td><b>Comentario</b></td><td colspan="2"><textarea  class="comentario" onkeyup="saveCommentsForSelectedCandidate()" ></textarea></td>
			</tr>
		        <tr> 
		        	<td colspan="3"> &nbsp </td> 
		        </tr>
		        
		        <?php endforeach; ?>
	        
	        <? } ?>
	        
	</table>
	
	<?php 
	

	endforeach; ?>
				

<script>

var candidateList = document.getElementById("candidato");

var guardarButton = document.getElementById("guardar");

var candidateIndex = -1;

var candidateQuestions = {};

var candidateVideoLinks = {};

var candidateComments = {};

var candidatesCount = <?php echo $candidatesCount ;?>;

function selectCandidate(element)
{	
	if(candidateIndex != -1)
	{
		document.getElementById("preguntas_" + candidateIndex).style.display = "none";
	}
	
	if(element.selectedIndex != 0)
	{
		guardarButton.style.display = "block";
	
		candidateIndex = element.selectedIndex;
		
		document.getElementById("preguntas_" + candidateIndex ).style.display = "block";	
		
		saveQuestionsForSelectedCandidate();
		saveVideoLinksForSelectedCandidate();
		saveCommentsForSelectedCandidate();
	}
	else
	{
		guardarButton.style.display = "none";
	}
}

function saveQuestionsForSelectedCandidate()
{
	var tableQuestions = document.getElementById("preguntas_" + candidateIndex );
	
	var questionsID = tableQuestions.getElementsByClassName("question_id");
	
	var questionsJSON = {};
	
	for(var i = 0; i < questionsID.length ; ++i)
	{
		questionsJSON[questionsID[i].value] = "";
		
		var answers = tableQuestions.getElementsByClassName("opcion_" + candidateList.value + "_" + questionsID[i].value);
		
		for(var a = 0; a < answers.length; ++a)
		{
			if(answers[a].checked)
			{
				questionsJSON[questionsID[i].value] = answers[a].value;
//				console.log(a);
				break;
			}
		}
	}
	
	//console.log(questionsJSON);
	
	//index 0 -> Seleccionar
	candidateQuestions[candidateList.value] = questionsJSON;
}

function saveVideoLinksForSelectedCandidate()
{
	var tableQuestions = document.getElementById("preguntas_" + candidateIndex );
	
	var questionsID = tableQuestions.getElementsByClassName("question_id");
	
	var videoLinks = tableQuestions.getElementsByClassName("link_video");
	
	var videoLinksJSON = {};
	
	for(var i = 0; i < questionsID.length ; ++i)
	{
		videoLinksJSON[questionsID[i].value] = videoLinks[i].value;
	}
	
	//console.log(questionsJSON);
	
	//index 0 -> Seleccionar
	candidateVideoLinks[candidateList.value] = videoLinksJSON;
}

function saveCommentsForSelectedCandidate()
{
	var tableQuestions = document.getElementById("preguntas_" + candidateIndex );
	
	var questionsID = tableQuestions.getElementsByClassName("question_id");

	var comments = tableQuestions.getElementsByClassName("comentario");
		
	var commentsJSON = {};
		
	for(var i = 0; i < questionsID.length ; ++i)
	{
		commentsJSON[questionsID[i].value] = comments[i].value;
	}
	
	//console.log(questionsJSON);
	
	//index 0 -> Seleccionar
	candidateComments[candidateList.value] = commentsJSON;
}

function finish()
{
	var form = document.createElement("form");
			
	form.method = "POST";
	form.action = "http://gpselectoral.pe/respuestas-candidato/";
	
	var inputCandidateQuestions = document.createElement("input");
	inputCandidateQuestions.name = "candidateQuestions";
	inputCandidateQuestions.type = "hidden";
	inputCandidateQuestions.value = JSON.stringify(candidateQuestions);
		
	var inputCandidateVideoLinks = document.createElement("input");
	inputCandidateVideoLinks.name = "candidateVideoLinks";
	inputCandidateVideoLinks.type = "hidden";
	inputCandidateVideoLinks.value = JSON.stringify(candidateVideoLinks);
		
	var inputCandidateComments = document.createElement("input");
	inputCandidateComments.name = "candidateComments";
	inputCandidateComments.type = "hidden";
	inputCandidateComments.value = JSON.stringify(candidateComments);

	form.appendChild(inputCandidateQuestions);
	form.appendChild(inputCandidateVideoLinks);
	form.appendChild(inputCandidateComments);
	
	form.submit();	
}


</script>
</div>

<?php get_footer(); ?>

