<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/custom-test.css">
<div class="box-pregunta open-box-preg">
<div class="wrap-question">
<script src ="<?php echo get_template_directory_uri(); ?>/js/ubigeo.js"></script>


<?php  global $wpdb;

	$departamentos = $wpdb->get_results( 'SELECT id_ubigeo, id_departamento, departamento FROM ubigeo group by id_departamento', OBJECT ); ?>



	 		<span class="back-white"></span>

	 		<?php $j = 1; ?>

	 		<?php 

	            $cant_preguntas = query_posts(array(

	                'post_type' => 'preguntas',

	                'showposts' => 50,

	                'orderby' => 'menu_order',

	                'order'   => 'ASC'

	            ) );  
	            $cant_preguntas = count($cant_preguntas);
	        ?>
<style type="text/css">
	.slick-next:after{
		content: " /<?php echo $cant_preguntas ?>";
	}

</style>
<script>

	var QUESTION_IMPORTANCE = {LOW : 0, MID: 1, HIGH : 2};

	var QUESTION_OPINION = {NOT_AGREE: 'desacuerdo', NEUTRAL : 'neurtal', AGREE : 'de_acuerdo'};

	

	var userData = {departamento_id:"", departamento : "", provincia_id:"", provincia : "", distrito_id : "", distrito : "", genero : "", edad : "",  grado : ""};

	

	var userQuestionResults = {};

	

	var questionElementsAnswered = [];

	var cantidad_total = <?php echo $cant_preguntas; ?>;

	var temp_index_page = 0;

	function skipUbigeo()	

	{

		$('#preg-1 > span').trigger('click');

		$(".thumb-nav").animate({opacity: 1}, 500);

		

	}

	

	function setUserData()

	{
		if ( $('#form').parsley().isValid() ) {

			var form = document.getElementById("form");

			console.log("SET_USER_DATA");

			

			userData.departamento_id = form.departamento.value;

			userData.provincia_id = form.provincia.value;

			userData.distrito_id = form.distrito.value;

			userData.departamento = form.departamento.selectedOptions[0].text;

			userData.provincia = form.provincia.selectedOptions[0].text;

			userData.distrito = form.distrito.selectedOptions[0].text;

			userData.genero = form.genero.value;

			userData.edad = form.edad.value;

			userData.grado = form.grado.value;

			

			$(".thumb-nav").fadeTo();
			$(".box-pregunta").removeClass("open-box-preg");


			console.log(userData);

			$(".thumb-nav").animate({opacity: 1}, 500);
			
		};
			

	}

	

	function saveOpinionForQuestion(questionNumber, opinion, questionId)

	{

		var $imp = parseInt( $("#container-"+questionNumber+" .importancia .active").html() );


		userQuestionResults[questionId] = {answered : true, importance : $imp, opinion : opinion, question_id: questionId};

		questionElementsAnswered[questionNumber] = true;

		if ( questionNumber < cantidad_total ) {

			

			for(var i = questionNumber + 1; i <= cantidad_total; ++i )

			{

				if( document.getElementById("preg-" + i) != null)

				{

					$('#preg-'+ i + ' > span').trigger('click');


					return $('.thumb-nav').slick('slickNext');

					break;

				}

			}

		};
			
		if ( questionNumber == cantidad_total ) {

			var answeredQuestionsCount = 0;

            if( ObjectLength( userQuestionResults ) < 20)

            {

                alert("Necesita responder " + (20 - ObjectLength( userQuestionResults ) ) +" pregunta(s) más para continuar_/");
                temp_index_page = 1;

            }

            else

            {   



                $(".thumb-nav").hide(); 

                for(var result in userQuestionResults)

                {

                    delete userQuestionResults[result].answered;

                }

                $(".last-quest").find("#userQuestionResults").val( JSON.stringify(userQuestionResults) );
                $(".last-quest").find("#userData").val( JSON.stringify(userData) );
                

                $(".last-quest").submit();
                console.log("enviar formulario!");
                $(".last-quest").serialize();

            }


		}
			

	}

	function postToFeed(picture, title, summary){
		var obj = {method: 'feed',link: '<?php echo site_url()."/test/"; ?>', picture: picture,name: title, description: summary};
		function callback(response){}
		FB.ui(obj, callback);
	}
	function saveImportanceForQuestion(questionNumber, importance, element)

	{

		console.log(questionNumber, importance);

		

		var importanceOptions = element.parentElement.getElementsByTagName("a");

		

		importanceOptions[0].className = "baja";

		importanceOptions[1].className = "media";

		importanceOptions[2].className = "alta";

		

		element.className += " active";				

	}

		
	function ObjectLength( object ) {
        var length = 0;
        for( var key in object ) {
            if( object.hasOwnProperty(key) ) {
                ++length;
            }
        }
        return length;
    };	

	function finish(form)

	{

		event.preventDefault();

		var answeredQuestionsCount = 0;


		if(answeredQuestionsCount < 20)

		{

			alert("Necesita responder " + (20 - answeredQuestionsCount ) +" pregunta(s) más para continuar");

			

			return false;

		}

		else

		{	

			$(".thumb-nav").hide();	

			for(var result in userQuestionResults)

			{

				delete userQuestionResults[result].answered;

			}

			return true;				

		}

	}
</script>	 	        

	        <nav class="thumb-nav">

	        	

		        	<a data-container="container-0" class="thumb-nav__item formulario" href="#" title="Registra tus datos"><span>Tus Datos</span></a>

			        <?php while (have_posts()) : the_post(); ?>



			  			<a data-container="container-<?php echo $j; ?>" id="preg-<?php echo $j; ?>" class="thumb-nav__item" href="#" title="<?php the_title(); ?>"><span><?php echo $j; ?></span></a>

			  			<?php $j++; ?>

			        <?php endwhile;?>

			    

	        </nav>

	    	<?php wp_reset_query(); ?>



	 		

	    	



	    	<div id="container-0" class="container theme-1 box-form">

				<header class="intro">

					

					<div class="intro__content">

						<form id="form" data-parsley-validate="">

							<span class="falta">Falta<br> <strong>

							<?php

							$datetime1 = new DateTime("now");

							$datetime2 = date_create('2016-04-10');

							$interval = date_diff($datetime1, $datetime2);

							echo $interval->format('%a%'); ?>

                            </strong><br> dias</span>

							<h2>Antes de empezar</h2>

							<div class="group-control">

								<label>¿De donde nos visitas?</label>

								<select id="departamento" name="departamento" required>

									

									

								</select>

								<select id="provincia" name="provincia" required>

									<option  value=''>[Seleccione Provincia]</option>

								</select>

								<select id="distrito" name="distrito" required>

									<option  value=''>[Seleccione Distrito]</option>

								</select>

							</div>

							<div class="group-control genero">

								<label class="lbl-left">¿Cuál es tu género?</label>

									<input type="radio" name="genero" id="masculino" class="masculino" checked="checked" value="M" required />

									<label for="masculino">Masculino</label>

									<input type="radio" name="genero" id="femenino" class="femenino" value="F" required />

									<label for="femenino">Femenino</label>

							</div>

							<div class="group-control two-col">

								<label>¿Qué edad tienes?</label>

								<input type="text" placeholder="Ingresa tu edad" name="edad" required data-parsley-type="digits" maxlength="3" data-parsley-min="15" data-parsley-max="100" />

							</div>

							<div class="group-control two-col">

								<label>Grado de instrucción</label>

								<select name="grado" id="grado" required >

									<option  value=''>[Seleccione Grado]</option>

									<option value="universitaria_completa">Universitaria completa</option>

									<option value="universitaria_incompleta">Universitaria incompleta</option>

									<option value="tecnico_incompleto">Técnico completo</option>

									<option value="tecnico_incompleto">Técnico incompleto</option>

									<option value="secundaria_completa">Secundaria completa</option>

									<option value="secundaria_incompleta">Secundaria incompleta</option>

									<option value="primaria_completa">Primaria completa</option>

									<option value="primaria_incompleta">Primaria incompleta</option>

								</select>

							</div>

							

							<div>

								<p><a class="thumb-nav__item omitir" onclick="skipUbigeo()" href="#">Saltar formulario &#187;</a></p>

							</div>



							<div class="group-control">

								<button type="submit" onclick="setUserData()">¡Empezar!</button>

							</div>

						</form>

					</div><!-- /intro__content -->

				</header><!-- /intro -->

				

			</div><!-- /container -->



			

			<?php $i = 1; ?>

			<?php 

	            $cant_preguntas = query_posts(array(

	                'post_type' => 'preguntas',

	                'showposts' => 50,

	                'orderby' => 'menu_order',

	                'order'   => 'DESC'

	            ) );  

	            $cant_preguntas = count($cant_preguntas);
	        ?>

	        

		<?php while (have_posts()) : the_post(); ?>

			<div id="container-<?php echo $i; ?>" class="container theme-1">

				<header class="intro">

					

					<div class="intro__content">

						<h1><?php the_title(); ?></h1>

						<h3>¿Cuán importante es para ti esta pregunta?</h3>

							

						<p class="importancia">

							<a href="#" class="baja" onclick="saveImportanceForQuestion(<?php echo $i - 1; ?>,0,this)">1</a>

							<a href="#" class="media active" onclick="saveImportanceForQuestion(<?php echo $i - 1; ?>,1,this)">2</a>

							<a href="#" class="alta" onclick="saveImportanceForQuestion(<?php echo $i - 1; ?>,2,this)">3</a>

						</p>

						

						<?php if($i < $cant_preguntas){ ?>

						

						<button class="btn btn-1 btn-1a" data-index="<?php echo $i; ?>" onclick="saveOpinionForQuestion(<?php echo $i; ?>,'de_acuerdo', <?php echo the_ID();?> )">DE ACUERDO</button>

						<button class="btn btn-1 btn-1a" data-index="<?php echo $i; ?>" onclick="saveOpinionForQuestion(<?php echo $i; ?>,'neutral',<?php echo the_ID();?>)">NEUTRAL</button>

						<button class="btn btn-1 btn-1a" data-index="<?php echo $i; ?>" onclick="saveOpinionForQuestion(<?php echo $i; ?>,'desacuerdo', <?php echo the_ID();?>)">EN DESACUERDO</button>

						

						<?php }else{ ?>

						
						<!--onsubmit="finish(this)"-->
						<form action = "<?php echo site_url(); ?>/afinidad?source=test" method="POST"  class="last-quest">

						<button class="btn btn-1 btn-1a" type="button" onclick="saveOpinionForQuestion(<?php echo $i ; ?>,'de_acuerdo', <?php echo the_ID();?> )">DE ACUERDO</button>

						<button class="btn btn-1 btn-1a" type="button" onclick="saveOpinionForQuestion(<?php echo $i ; ?>,'neutral',<?php echo the_ID();?>)">NEUTRAL</button>

						<button class="btn btn-1 btn-1a" type="button" onclick="saveOpinionForQuestion(<?php echo $i ; ?>,'desacuerdo', <?php echo the_ID();?>)">EN DESACUERDO</button>

						<input type="hidden" name="userQuestionResults" value="" id="userQuestionResults">
						<input type="hidden" name="userData" value="" id="userData">

						</form>				

						<?php } ?>

						

						<p><a class="thumb-nav__item omitir" onclick="$('#preg-<?php echo ($i+1); ?> > span').trigger('click');" href="#">Omitir &#187;</a></p>

					</div><!-- /intro__content -->

					

				</header><!-- /intro -->

				

			</div><!-- /container -->
			

			<?php $i++; ?>

		<?php endwhile;?>

		

		<?php wp_reset_query(); ?>



		<?php $i = 1; ?>

		<?php 

            $cant_preguntas = query_posts(array(

                'post_type' => 'preguntas',

                'showposts' => 50,

                'orderby' => 'menu_order',

                'order'   => 'ASC'

            ) );  

            $cant_preguntas = count($cant_preguntas);
        ?>

	        
        <div class="box-rpts box-resumen" style="display:none;">

			<?php while (have_posts()) : the_post(); ?>
				
				
				<div id="rpts-<?php echo $i; ?>" class="box Wrapper">
					<h3>
						<span></span> <a href="javascript:;"> <?php the_title(); ?> </a>
						<small style="display: block;color: #333;font-size: 10px;margin-top:5px;text-transform: none;">(Dale click a la foto de los candidatos para ver la explicación)</small>
					</h3>
					<?php 
			        	$id_pregunta = get_the_id();
			        	global $wpdb;
						$results = $wpdb->get_results( 'SELECT id_candidato, preguntas, links, comentarios FROM candidato_preguntas', OBJECT );
			         	//shuffle($results);
			         	foreach ($results as $key => $value) {

			         		$respuestas = json_decode($value->preguntas, true);
			         		$comentarios = json_decode($value->comentarios, true);
			         		$link_youtube = json_decode($value->links, true);
			         		$txt ="";
			         		$video ="";
			         		if ( $comentarios != null ) {
			         			$txt = array_key_exists( $id_pregunta , $comentarios ) ? $comentarios[$id_pregunta]:"";
			         		}
			         		if ( $link_youtube != null ) {
			         			$video = array_key_exists( $id_pregunta , $link_youtube ) ? $link_youtube[$id_pregunta]:"";
			         		}

			         		$foto_candidato = get_field('foto_perfil', $value->id_candidato ); 
		         			$nombre_candidato =get_post( $value->id_candidato )->post_title;
		         			$partido_candidato =get_field('partido', $value->id_candidato );
				         	
				         	if ( trim($txt) != "" || trim($video) != "" ) {
				         		
				         		//Imprimir los Candidatos que respondieron
				         		?>
				         		<?php //if ($video!= "") { ?>
				         			<section class="resumen-respuestas <?php echo $respuestas[$id_pregunta]; ?> <?php echo trim($respuestas[$id_pregunta]) == "" ? "neutral":""; ?>">
				         				<?php 
				         					
				         						$parts = parse_url($video);
												parse_str($parts['query'], $query);
												$v = $query['v']; ?>
				         				<aside>
				         					<?php if ( trim( $v ) != "") { ?>
					         					<a href="http://www.youtube.com/embed/<?php echo $v; ?>?autoplay=1" class="video-rpta fancybox.iframe no-style"><span class="icon-video"></span> 
					         						<img src="<?php echo $foto_candidato; ?>">
					         					</a>
					         				<?php }else{ ?>

												<a href="#inline" class="various-inline no-style"><img src="<?php echo $foto_candidato; ?>"></a>
												<div style="display:none;"><?php echo $txt; ?></div>
											<?php } ?>
				         					<h4><?php echo $nombre_candidato; ?></h4>
				         					<h5><?php echo $partido_candidato; ?></h5>
				         				</aside>
				         				<article>
				         					<?php echo $txt; ?>
				         				</article>
				         					<?php 

				         						$rpta_c = "";

			         							if ($respuestas[$id_pregunta] == "de_acuerdo") {
			         								$rpta_c = "De acuerdo";
			         							}
			         							if ($respuestas[$id_pregunta] == "desacuerdo") {
			         								$rpta_c = "En desacuerdo";
			         							}
			         							if ($respuestas[$id_pregunta] == "neutral") {
			         								$rpta_c = "Neutral";
			         							}
			         							if ( trim($respuestas[$id_pregunta]) == "") {
			         								$rpta_c = "No respondió";
			         							}

			         						?>
				         					<?php if ( trim( $v ) != "") { ?>
					         					
					         					<a href="http://www.youtube.com/embed/<?php echo $v; ?>?autoplay=1" class="video-rpta fancybox.iframe">
					         						<span class="icon-video"></span>
					         						<?php echo $rpta_c; ?>
					         					</a>

											<?php }else{ ?>

												<a href="#inline" class="various-inline"><?php echo $rpta_c; ?></a>
												<div style="display:none;"><?php echo $txt; ?></div>
											<?php } ?>
				         			</section>

				         		<?php //} ?>

				         		<?php
				         	}
			         	}

			          ?>
			    </div> 
				

				<?php $i++; ?>

			<?php endwhile;?>
		</div>

<div id="inline">
	
</div>
		<div id="comentarios"></div>


		<script src="<?php echo get_template_directory_uri(); ?>/js/classie.js"></script>

		<script>


		</script>
	</div>
</div>









<?php get_footer(); ?>