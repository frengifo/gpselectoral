<?php 
if ( !isset( $_POST['userQuestionResults'] ) ) {
	header("Location: ".site_url());
} ?>
<?php get_header(); ?>
<script src ="<?php echo get_template_directory_uri(); ?>/js/ubigeo.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/unslider.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/unslider-dots.css">
<span style="display:none;">
	<?php  var_dump($_POST); ?>
</span>
<?php

global $wpdb;

$wpdb->insert("usuario", array("datos_usuario"  => stripslashes($_REQUEST["userData"])));

$userId = $wpdb->insert_id;  

$userQuestionResultsString = stripslashes($_REQUEST["userQuestionResults"]);

$userQuestionResultsJSON = json_decode($userQuestionResultsString ,true);

$questionCount = count($userQuestionResultsJSON);

$results = [];

$candidates = $wpdb->get_results('SELECT post_title,post_name,ID,guid FROM wpor_posts where  post_type = "candidatos" and post_status = "publish" order by ID DESC', OBJECT);

$candidateQuestions = $wpdb->get_results('SELECT id_candidato, preguntas FROM candidato_preguntas order by id_candidato DESC', OBJECT);



//print_r($candidateQuestions);

foreach($candidateQuestions as $candidateQuestion)
{
	$q = json_decode($candidateQuestion->preguntas, true);
	
	$results[$candidateQuestion->id_candidato] = 0;
	
	foreach($q as $key => $opinion)
	{	
		if($userQuestionResultsJSON[$key]["opinion"] == $opinion)
		{
			$results[$candidateQuestion->id_candidato]++;			
		}		
	}
}

$sortedResults = [];

foreach($results as $key => $value)
{
	$sortedResults [] = array($key, ((float)$value/(float)$questionCount)*100.0 );	
}


for($i = 0; $i < count($sortedResults ); ++$i)
{		
	for($j = $i + 1; $j < count($sortedResults ); ++$j)
	{
		if($sortedResults[$i][1]< $sortedResults[$j][1])	
		{
			$temp = $sortedResults[$j];
			
			$sortedResults[$j][0] = $sortedResults[$i][0];
			$sortedResults[$j][1] = $sortedResults[$i][1];
			
			$sortedResults[$i][0] = $temp [0];
			$sortedResults[$i][1] = $temp [1];			
		}
	}
}


$wpdb->insert("usuario_resultados", array("id_usuario"  => $userId,"respuestas" => $userQuestionResultsString, "afinidad" => json_encode($sortedResults )));

$imagesCandidates = $wpdb->get_results('SELECT guid,ID,post_name FROM wpor_posts where post_type = "attachment" order by ID DESC', OBJECT);

$partidos = $wpdb->get_results('SELECT * FROM `wpor_postmeta` where meta_key = "partido"', OBJECT);

//extraer candidatos en orden

function getPartido($partidos ,$idCandidato)
{
	foreach($partidos as $partido)
	{
		if($partido->post_id == $idCandidato)
			return $partido->meta_value;
	}
	
	return "";
}

function getCandidate($candidates,$id)
{
	foreach($candidates as $candidate)
	{
		if($candidate->ID == $id)
			return $candidate;
	}
	
	return null;
}

function getCandidateImageURLWithName($imagesCandidates,$name)
{
	$findDashes = explode("-",$name);
	
	if(count($findDashed) == 1)
	{
		$findDashes = explode("_",$name);		
	}
	
	foreach($imagesCandidates as $imagesCandidate)
	{
		//echo $imagesCandidate->post_name." ". $findDashes[0]."<br>";

		if (strpos($imagesCandidate->guid, $findDashes[0]) !== FALSE)
			return $imagesCandidate->guid;
	}
	
	return "";
}
function getImagesCandidate($id){

	// retrieve one post with an ID of 5
	query_posts( 'p='.$id );
	$images = array();
	// set $more to 0 in order to only get the first part of the post
	global $more;
	$more = 0;
	// the Loop
	while (have_posts()) : the_post();
		array_push($images, get_field("foto_perfil"));
		array_push($images, get_field("foto_share"));
	endwhile;
	wp_reset_query();

	return $images;
}

?>				
<style type="text/css">
 
  div.box-pregunta{
  	min-height: 100vh;
    background-size: 100% 95%;
    background-position: center 5em;
  }
  #container-0.container, .container::after{
  	    height: 85vh;
  }
  #container-0 #form{
  	    margin-top: 4em;
    padding: 0em 6em 1em 3em;
  }
  .container, .container::after{
  	    height: 100vh
  }

  .back-white{
  	height: 70px
  }
  .thumb-nav{
  	top: 5.5em;
  }
  header{
	padding: 0em 0;
	display: none;
  }    
  .social{
  	margin-top: 15em;
  }
  body{
  	    overflow-y: hidden;
  }
  .logo{
  	max-width: 130px
  }
  .falta{
  	background-color: rgba(17, 17, 17, 0.78);
  }
  .menu{
  	display: none;
  }
  .wrap-question {
	    width: 660px;
	    margin-left: -330px;
	}
  div#nav-icon4.sb-toggle-right{
  	display: none !important;
  }
  footer{
  	display: none;
  }
  #form button{
  	background-color: rgb(17, 17, 17);
  }
  .view-rpts{
  	display: none !important;
  }
  .intro__content {
	    position: absolute;
	    width: 580px;
	    top: 50%;
	    padding: 0em 1em 0 1em;
	    margin: 0 5%;
	    font-size: .95em;
	    background-color: rgba(255, 255, 255, 0.86);
	    box-shadow: 1px 1px 4px 4px rgba(0, 0, 0, 0.27);
	    /* padding-bottom: 2em; */
	    /* height: 100%; */
	    text-align: center;
	    left: 50%;
	    margin-left: -303px;
	    top: 55%;
	    -webkit-transform: translateY(-50%);
	    -ms-transform: translateY(-50%);
	    transform: translateY(-50%);
	}
	.intro, .container{
		overflow: visible;
	}
#form .femenino + span {
    font-size: .95em;
}
#form .masculino + span {
    font-size: .95em;
    margin-right: 1.5em;
}
#form label, #form select, #form input[type="text"]{
	    font-size: .9em;
}
#form .genero{
	margin: 0.5em 0;
}
#form input[type="radio"]{
	margin: 0;
}
#form input[type="text"]{
	width: 56.2%;
    margin-bottom: .5em;
}
#form .group-control{
	    padding: 0 0 0;
}
#form select{}
#form > h2 {
    font-size: 1.2em;
    font-weight: 600;
}

  @media screen and (max-width: 1024px) {
  	.container, .container::after{

  	}
  	div.box-pregunta{
	  	min-height: 80vh;
	    background-size: 140% 95%;
	    background-position: center 5em;
	}
	#container-0.container, .container::after{}
  }
  @media screen and (max-width: 769px) {
  	.container, .container::after{
  		height: 75vh;
  	}
  	div.box-pregunta{
	  	min-height: 70vh;
	    background-size: 135% 90%;
	    background-position: center 5em;
	}
	#container-0.container, .container::after{}
	#container-0 #form{}
  }
  @media screen and (max-width: 680px) {
  	.thumb-nav{
  		height: 60px;
  		    top: 15%;
  	}
  	.container, .container::after{
  		height: 48vh;
  	}
  	div.box-pregunta{
	  	min-height: 55vh;
	    background-size: 200%;
	    background-position: 25% 4em;
	}
	#container-0.container, .container::after{
		height: 52vh;
	}
	.logo{
		    max-width: 120px;
	}
	#container-0 #form{}
  }
  @media screen and (max-width: 480px){
  	.container, .container::after{
  		height: 50vh;
  	}
  	div.box-pregunta{
	  	min-height: 55vh;
	    background-size: cover;
	    background-position: 25% 4em;
	}
	.fancybox-iframe{
		max-height: 350px;
	}
	#container-0 #form{}
  }

  </style>
	<div class="box-resultado">
		
		<a href="<?php echo site_url(); ?>/widget" style="color: #fff;padding: .5em 1em;background-color: #222222;display: inline-block;font-size: .8em;border-radius: 4px;position: absolute;margin: 5px;top: .1em;"><i class="fa fa-undo"></i> Regresar al test</a>
       
		<div class="promos"> 
			<h2 class="title-afinidad">Tienes más afinidad con estos candidatos</h2> 
		    <div class="promo first">
		        <h4>  <?php echo (int)($sortedResults[2][1]); ?>%</h4>
		        <!--<img src="<?php echo getCandidateImageURLWithName($imagesCandidates ,getCandidate($candidates,$sortedResults[2][0])->post_name); ?>">-->
		        <?php $foto_perfil = get_field('foto_perfil', getCandidate($candidates,$sortedResults[2][0])->ID ); ?>
		        <?php $foto_partido = get_field('foto_partido', getCandidate($candidates,$sortedResults[2][0])->ID ); ?>
		        <img src="<?php echo $foto_perfil; ?>"> <img src="<?php echo $foto_partido; ?>" class="foto-partido-w">
		        <h2> <?php echo getCandidate($candidates,$sortedResults[2][0])->post_title; ?></h2>
		        <p>
				<?php echo getPartido($partidos , $sortedResults[2][0]) ;?>
				
		        </p>
		       	<div class="share-social">
		        	<?php
					    $title = ("Tengo un ".(int)($sortedResults[2][1])."% de compatibilidad con ".getCandidate($candidates,$sortedResults[2][0])->post_title);
					    $url = ( site_url()."/test/" );
					    $summary = ("Quieres saber con que candidato tienes más afinidad, dale click aquí y averigualo.");
					    $twitter_summary = ".Haz el test aqui ";
					    $image = get_field('foto_share', getCandidate($candidates,$sortedResults[2][0])->ID);
					?>
		        	<a href="javascript:;" 
		        		onclick="postToFeed('<?php echo $image ?>', '<?php echo $title; ?>', '<?php echo $summary; ?>');" class="share-icon">
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/facebook_share.png">
		        	</a>
		        	<a href="javascript:;" onclick="window.open('https://twitter.com/intent/tweet?url=<?php echo site_url()."/test/"; ?>&amp;text=<?php echo urlencode($title)." ".urlencode($twitter_summary); ?>&amp;via=gpselectoral','facebook-share-dialog','width=626,height=436')" class="share-icon" >
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/twitter_share.png">
		        	</a>
		        </div>
		    </div>
		    <div class="promo second">
		        <h4><?php echo (int)($sortedResults[1][1]); ?>%</h4>
		        <!--<img src="<?php echo getCandidateImageURLWithName($imagesCandidates ,getCandidate($candidates,$sortedResults[1][0])->post_name); ?>">-->
		        <?php $foto_perfil = get_field('foto_perfil', getCandidate($candidates,$sortedResults[1][0])->ID ); ?>
		        <?php $foto_partido = get_field('foto_partido', getCandidate($candidates,$sortedResults[1][0])->ID ); ?>
		        <img src="<?php echo $foto_perfil; ?>"> <img src="<?php echo $foto_partido; ?>" class="foto-partido-w">
		        <h2><?php echo getCandidate($candidates,$sortedResults[1][0])->post_title; ?></h2>
		        <p> <?php echo getPartido($partidos , $sortedResults[1][0]) ;?></p>
		        <div class="share-social">
		        	<?php
					    $title = ("Tengo un ".(int)($sortedResults[1][1])."% de compatibilidad con ".getCandidate($candidates,$sortedResults[1][0])->post_title);
					    $url = ( site_url()."/test/" );
					    $summary = ("Quieres saber con que candidato tienes más afinidad, dale click aquí y averigualo.");
					    $image = get_field('foto_share', getCandidate($candidates,$sortedResults[1][0])->ID);
					?>
		        	<a href="javascript:;" 
		        		onclick="postToFeed('<?php echo $image ?>', '<?php echo $title; ?>', '<?php echo $summary; ?>');" class="share-icon">
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/facebook_share.png">
		        	</a>
		        	<a href="javascript:;" onclick="window.open('https://twitter.com/intent/tweet?url=<?php echo site_url()."/test/"; ?>&amp;text=<?php echo urlencode($title)." ".urlencode($twitter_summary); ?>&amp;via=gpselectoral','facebook-share-dialog','width=626,height=436')" class="share-icon">
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/twitter_share.png">
		        	</a>
		        </div>

		    </div>
		    <div class="promo third scale ganador">
		        <h4><?php echo (int)($sortedResults[0][1]); ?>%</h4>
		        <!--<img src="<?php echo getCandidateImageURLWithName($imagesCandidates ,getCandidate($candidates,$sortedResults[0][0])->post_name); ?>">-->
		        <?php $foto_perfil = get_field('foto_perfil', getCandidate($candidates,$sortedResults[0][0])->ID ); ?>
		        <?php $foto_partido = get_field('foto_partido', getCandidate($candidates,$sortedResults[0][0])->ID ); ?>
		        <img src="<?php echo $foto_perfil; ?>"> <img src="<?php echo $foto_partido; ?>" class="foto-partido-w">
		        <h2><?php echo getCandidate($candidates,$sortedResults[0][0])->post_title; ?></h2>
		        <p> <?php echo getPartido($partidos , $sortedResults[0][0]) ;?></p>
		        <div class="share-social">
		        	<?php
					    $title = ("Tengo un ".(int)($sortedResults[0][1])."% de compatibilidad con ".getCandidate($candidates,$sortedResults[0][0])->post_title);
					    $url = ( site_url()."/test/" );
					    $summary = ("Quieres saber con que candidato tienes más afinidad, dale click aquí y averigualo.");
					    $image = get_field('foto_share', getCandidate($candidates,$sortedResults[0][0])->ID);;
					?>
		        	<a href="javascript:;" 
		        		onclick="postToFeed('<?php echo $image ?>', '<?php echo $title; ?>', '<?php echo $summary; ?>');" class="share-icon">
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/facebook_share.png">
		        	</a>
		        	<a href="javascript:;" onclick="window.open('https://twitter.com/intent/tweet?url=<?php echo site_url()."/test/"; ?>&amp;text=<?php echo urlencode($title)." ".urlencode($twitter_summary); ?>&amp;via=gpselectoral','facebook-share-dialog','width=626,height=436')" class="share-icon" >
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/twitter_share.png">
		        	</a>
		        </div>
		    </div>  
		</div>
		<script type="text/javascript">

	        function postToFeed(picture, title, summary){
				var obj = {method: 'feed',link: '<?php echo site_url()."/test/"; ?>', picture: picture,name: title, description: summary};
				function callback(response){}
				FB.ui(obj, callback);
			}

        </script>
		<!--
		<div class="promos responsive">  

		    <div class="promo third scale ganador">
		        <h4>63%</h4>
		        <img src="http://gpselectoral.pe/wp-content/uploads/2016/01/julio_guzman.jpg">
		        <h2>Julio Guzmán</h2>
		        <p>Todos por el Perú</p>
		    </div>  
		</div>
		
		-->
	</div>
	<style type="text/css">
	.todos{
		display: none !important;
	}
	</style>
	<section class="todos">
		<h2><span>Todos los candidatos</span></h2>
		
			<?php for($i = 3; $i < count($sortedResults); ++$i): ?>
				
					<article>
						<ul>
							<li><strong><?php echo getCandidate($candidates,$sortedResults[$i][0])->post_title; ?></strong><br><small><?php echo getPartido($partidos , $sortedResults[$i][0]) ;?></small></li>
							<li>
								<!---->
								<img src="<?php echo get_field('foto_perfil', getCandidate($candidates,$sortedResults[$i][0])->ID); ?>">
							</li>
							<li class="puntuacion"><?php echo (int)($sortedResults[$i][1]); ?>%</li>
							
						</ul>
					</article>
				
			<?php endfor; ?>
		

			
	</section>

	<!--Mobile Listado Candidatos -->
	<section class="todos mobile">
		<h2><span>Todos los candidatos</span></h2>

		<?php for($i = 1; $i < count($sortedResults); ++$i): ?>
		<article>
			<ul>
				<li><strong><?php echo getCandidate($candidates,$sortedResults[$i][0])->post_title; ?></strong><br><small><?php echo getPartido($partidos , $sortedResults[$i][0]) ;?></small></li>
				<li><img src="<?php echo get_field('foto_perfil', getCandidate($candidates,$sortedResults[$i][0])->ID); ?>"></li>
				<li class="puntuacion"><?php echo (int)($sortedResults[$i][1]); ?>%</li>
				
			</ul>
		</article>
		
		<?php endfor; ?>
		
	</section>

	<section class="resumen Wrapper">
		<a href="http://gpselectoral.pe/test/" target="_blank" style="    position: absolute;    bottom: 2em;    right: 1em;    font-size: .8em;    z-index: 999999;"> Ir a la version Web</a>
		<h2><span>Resumen de respuestas</span></h2>
		<?php $i = 1; ?>

		<?php 

            $cant_preguntas = query_posts(array(

                'post_type' => 'preguntas',

                'showposts' => 50,

                'orderby' => 'menu_order',

                'order'   => 'ASC'

            ) );  

            $cant_preguntas = count($cant_preguntas);
        ?>

        <div class="counter" style="text-align:right;">
        	<span class="current">1</span> / <span class="last"> <?php echo $cant_preguntas; ?> </span>
        </div>
	        
        <div class="box-resumen">
        	<ul>
			<?php while (have_posts()) : the_post(); ?>
				<li>
					<div id="rpts-<?php echo $i; ?>" class="box">
						<h3>
							<span></span> <a href="javascript:;"> <?php the_title(); ?> </a>
							<small style="display: block;color: #333;font-size: 10px;margin-top:5px;text-transform: none;">(Dale click a la foto de los candidatos para ver la explicación)</small>
						</h3>
						<?php 
				        	$id_pregunta = get_the_id();
				        	global $wpdb;
							$results = $wpdb->get_results( 'SELECT id_candidato, preguntas, links, comentarios FROM candidato_preguntas', OBJECT );
				         	shuffle($results);
				         	foreach ($results as $key => $value) {

				         		$respuestas = json_decode($value->preguntas, true);
				         		$comentarios = json_decode($value->comentarios, true);
				         		$link_youtube = json_decode($value->links, true);
				         		$txt ="";
				         		$video ="";
				         		if ( $comentarios != null ) {
				         			$txt = array_key_exists( $id_pregunta , $comentarios ) ? $comentarios[$id_pregunta]:"";
				         		}
				         		if ( $link_youtube != null ) {
				         			$video = array_key_exists( $id_pregunta , $link_youtube ) ? $link_youtube[$id_pregunta]:"";
				         		}

				         		$foto_candidato = get_field('foto_perfil', $value->id_candidato ); 
			         			$nombre_candidato =get_post( $value->id_candidato )->post_title;
			         			$partido_candidato =get_field('partido', $value->id_candidato );
					         	
					         	if ( trim($txt) != "" || trim($video) != "" ) {
				         		
				         		//Imprimir los Candidatos que respondieron
				         		?>
				         		<?php //if ($video!= "") { ?>
				         			<section class="resumen-respuestas <?php echo $respuestas[$id_pregunta]; ?> <?php echo trim($respuestas[$id_pregunta]) == "" ? "neutral":""; ?>">
				         				<?php 
				         					
				         						$parts = parse_url($video);
												parse_str($parts['query'], $query);
												$v = $query['v']; ?>
				         				<aside>
				         					<?php if ( trim( $v ) != "") { ?>
					         					<a href="http://www.youtube.com/embed/<?php echo $v; ?>?autoplay=1" class="ver-video fancybox.iframe no-style"><span class="icon-video"></span> 
					         						<img src="<?php echo $foto_candidato; ?>">
					         					</a>
					         				<?php }else{ ?>

												<a href="#inline" class="various-inline no-style"><img src="<?php echo $foto_candidato; ?>"></a>
												<div style="display:none;"><?php echo $txt; ?></div>
											<?php } ?>
				         					<h4><?php echo $nombre_candidato; ?></h4>
				         					<h5><?php echo $partido_candidato; ?></h5>
				         				</aside>
				         				<article>
				         					<?php echo $txt; ?>
				         				</article>
				         					<?php 

				         						$rpta_c = "";

			         							if ($respuestas[$id_pregunta] == "de_acuerdo") {
			         								$rpta_c = "De acuerdo";
			         							}
			         							if ($respuestas[$id_pregunta] == "desacuerdo") {
			         								$rpta_c = "En desacuerdo";
			         							}
			         							if ($respuestas[$id_pregunta] == "neutral") {
			         								$rpta_c = "Neutral";
			         							}
			         							if ( trim($respuestas[$id_pregunta]) == "") {
			         								$rpta_c = "No respondió";
			         							}

			         						?>
				         					<?php if ( trim( $v ) != "") { ?>
					         					
					         					<a href="http://www.youtube.com/embed/<?php echo $v; ?>?autoplay=1" class="ver-video fancybox.iframe">
					         						<span class="icon-video"></span>
					         						<?php echo $rpta_c; ?>
					         					</a>

											<?php }else{ ?>

												<a href="#inline" class="various-inline"><?php echo $rpta_c; ?></a>
												<div style="display:none;"><?php echo $txt; ?></div>
											<?php } ?>
				         			</section>

				         		<?php //} ?>

				         		<?php
				         	}
				         	}

				          ?>
				    </div> 
					

					<?php $i++; ?>
				</li>
			<?php endwhile;?>

			</ul>
		</div>

	</section>
<div id="inline">
	
</div>
<?php get_footer(); ?>

