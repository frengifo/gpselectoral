<?php 
if ( !isset( $_POST['userQuestionResults'] ) ) {
	header("Location: ".site_url());
} ?>

<?php get_header(); ?>
<script src ="<?php echo get_template_directory_uri(); ?>/js/ubigeo.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/custom-result.css">

<?php

global $wpdb;

$wpdb->insert("usuario", array("datos_usuario"  => stripslashes($_REQUEST["userData"])));

$userId = $wpdb->insert_id;  

$userQuestionResultsString = stripslashes($_REQUEST["userQuestionResults"]);

$userQuestionResultsJSON = json_decode($userQuestionResultsString ,true);

$questionCount = count($userQuestionResultsJSON);

$results = [];

$candidates = $wpdb->get_results('SELECT post_title,post_name,ID,guid FROM wpor_posts where  post_type = "candidatos" and post_status = "publish" order by ID DESC', OBJECT);

$candidateQuestions = $wpdb->get_results('SELECT id_candidato, preguntas FROM candidato_preguntas order by id_candidato DESC', OBJECT);



//print_r($candidateQuestions);

foreach($candidateQuestions as $candidateQuestion)
{
	$q = json_decode($candidateQuestion->preguntas, true);
	
	$results[$candidateQuestion->id_candidato] = 0;
	
	foreach($q as $key => $opinion)
	{	
		if($userQuestionResultsJSON[$key]["opinion"] == $opinion)
		{
			$results[$candidateQuestion->id_candidato]++;			
		}		
	}
}

$sortedResults = [];

foreach($results as $key => $value)
{
	$sortedResults [] = array($key, ((float)$value/(float)$questionCount)*100.0 );	
}


for($i = 0; $i < count($sortedResults ); ++$i)
{		
	for($j = $i + 1; $j < count($sortedResults ); ++$j)
	{
		if($sortedResults[$i][1]< $sortedResults[$j][1])	
		{
			$temp = $sortedResults[$j];
			
			$sortedResults[$j][0] = $sortedResults[$i][0];
			$sortedResults[$j][1] = $sortedResults[$i][1];
			
			$sortedResults[$i][0] = $temp [0];
			$sortedResults[$i][1] = $temp [1];			
		}
	}
}


$wpdb->insert("usuario_resultados", array("id_usuario"  => $userId,"respuestas" => $userQuestionResultsString, "afinidad" => json_encode($sortedResults )));

$imagesCandidates = $wpdb->get_results('SELECT guid,ID,post_name FROM wpor_posts where post_type = "attachment" order by ID DESC', OBJECT);

$partidos = $wpdb->get_results('SELECT * FROM `wpor_postmeta` where meta_key = "partido"', OBJECT);

//extraer candidatos en orden

function getPartido($partidos ,$idCandidato)
{
	foreach($partidos as $partido)
	{
		if($partido->post_id == $idCandidato)
			return $partido->meta_value;
	}
	
	return "";
}

function getCandidate($candidates,$id)
{
	foreach($candidates as $candidate)
	{
		if($candidate->ID == $id)
			return $candidate;
	}
	
	return null;
}

function getCandidateImageURLWithName($imagesCandidates,$name)
{
	$findDashes = explode("-",$name);
	
	if(count($findDashed) == 1)
	{
		$findDashes = explode("_",$name);		
	}
	
	foreach($imagesCandidates as $imagesCandidate)
	{
		//echo $imagesCandidate->post_name." ". $findDashes[0]."<br>";

		if (strpos($imagesCandidate->guid, $findDashes[0]) !== FALSE)
			return $imagesCandidate->guid;
	}
	
	return "";
}
function getImagesCandidate($id){

	// retrieve one post with an ID of 5
	query_posts( 'p='.$id );
	$images = array();
	// set $more to 0 in order to only get the first part of the post
	global $more;
	$more = 0;
	// the Loop
	while (have_posts()) : the_post();
		array_push($images, get_field("foto_perfil"));
		array_push($images, get_field("foto_share"));
	endwhile;
	wp_reset_query();

	return $images;
}

?>				
<style type="text/css">
 
  

  </style>
	<div class="box-resultado">

		<a href="<?php echo site_url(); ?>/test" style="color: #fff;padding: .5em 1em;background-color: #222222;display: inline-block;font-size: .8em;border-radius: 4px;position: absolute;margin: 5px;top: 8.1em;"><i class="fa fa-undo"></i> Regresar al test</a>

		<div class="promos"> 
			<h2 class="title-afinidad">Tienes más afinidad con estos candidatos</h2> 
		    <div class="promo first">
		        <h4>  <?php echo (int)($sortedResults[2][1]); ?>%</h4>
		        <!--<img src="<?php echo getCandidateImageURLWithName($imagesCandidates ,getCandidate($candidates,$sortedResults[2][0])->post_name); ?>">-->
		        <?php $foto_perfil = get_field('foto_perfil', getCandidate($candidates,$sortedResults[2][0])->ID ); ?>
		        <?php $foto_partido = get_field('foto_partido', getCandidate($candidates,$sortedResults[2][0])->ID ); ?>
		        <div class="box-img"> <img src="<?php echo $foto_perfil; ?>"> <img src="<?php echo $foto_partido; ?>" class="foto-partido"> </div>
		        <h2> <?php echo getCandidate($candidates,$sortedResults[2][0])->post_title; ?></h2>
		        <p>
				<?php echo getPartido($partidos , $sortedResults[2][0]) ;?>
				
		        </p>
		       	<div class="share-social">
		        	<?php
					    $title = ("Tengo un ".(int)($sortedResults[2][1])."% de compatibilidad con ".getCandidate($candidates,$sortedResults[2][0])->post_title);
					    $url = ( site_url()."/test/" );
					    $summary = ("Quieres saber con que candidato tienes más afinidad, dale click aquí y averigualo.");
					    $twitter_summary = ".Haz el test aqui ";
					    $image = get_field('foto_share', getCandidate($candidates,$sortedResults[2][0])->ID);
					?>
		        	<a href="javascript:;" 
		        		onclick="postToFeed('<?php echo $image ?>', '<?php echo $title; ?>', '<?php echo $summary; ?>');" class="share-icon">
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/facebook_share.png">
		        	</a>
		        	<a href="javascript:;" onclick="window.open('https://twitter.com/intent/tweet?url=<?php echo site_url()."/test/"; ?>&amp;text=<?php echo urlencode($title)." ".urlencode($twitter_summary); ?>&amp;via=gpselectoral','facebook-share-dialog','width=626,height=436')" class="share-icon" >
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/twitter_share.png">
		        	</a>
		        </div>
		    </div>
		    <div class="promo second">
		        <h4><?php echo (int)($sortedResults[1][1]); ?>%</h4>
		        <!--<img src="<?php echo getCandidateImageURLWithName($imagesCandidates ,getCandidate($candidates,$sortedResults[1][0])->post_name); ?>">-->
		        <?php $foto_perfil = get_field('foto_perfil', getCandidate($candidates,$sortedResults[1][0])->ID ); ?>
		        <?php $foto_partido = get_field('foto_partido', getCandidate($candidates,$sortedResults[1][0])->ID ); ?>
		        <div class="box-img"><img src="<?php echo $foto_perfil; ?>"> <img src="<?php echo $foto_partido; ?>" class="foto-partido"></div>
		        <h2><?php echo getCandidate($candidates,$sortedResults[1][0])->post_title; ?></h2>
		        <p> <?php echo getPartido($partidos , $sortedResults[1][0]) ;?></p>
		        <div class="share-social">
		        	<?php
					    $title = ("Tengo un ".(int)($sortedResults[1][1])."% de compatibilidad con ".getCandidate($candidates,$sortedResults[1][0])->post_title);
					    $url = ( site_url()."/test/" );
					    $summary = ("Quieres saber con que candidato tienes más afinidad, dale click aquí y averigualo.");
					    $image = get_field('foto_share', getCandidate($candidates,$sortedResults[1][0])->ID);
					?>
		        	<a href="javascript:;" 
		        		onclick="postToFeed('<?php echo $image ?>', '<?php echo $title; ?>', '<?php echo $summary; ?>');" class="share-icon">
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/facebook_share.png">
		        	</a>
		        	<a href="javascript:;" onclick="window.open('https://twitter.com/intent/tweet?url=<?php echo site_url()."/test/"; ?>&amp;text=<?php echo urlencode($title)." ".urlencode($twitter_summary); ?>&amp;via=gpselectoral','facebook-share-dialog','width=626,height=436')" class="share-icon">
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/twitter_share.png">
		        	</a>
		        </div>

		    </div>
		    <div class="promo third scale ganador">
		        <h4><?php echo (int)($sortedResults[0][1]); ?>%</h4>
		        <!--<img src="<?php echo getCandidateImageURLWithName($imagesCandidates ,getCandidate($candidates,$sortedResults[0][0])->post_name); ?>">-->
		        <?php $foto_perfil = get_field('foto_perfil', getCandidate($candidates,$sortedResults[0][0])->ID ); ?>
		        <?php $foto_partido = get_field('foto_partido', getCandidate($candidates,$sortedResults[0][0])->ID ); ?>
		        <div class="box-img"><img src="<?php echo $foto_perfil; ?>"> <img src="<?php echo $foto_partido; ?>" class="foto-partido"></div>
		        <h2><?php echo getCandidate($candidates,$sortedResults[0][0])->post_title; ?></h2>
		        <p> <?php echo getPartido($partidos , $sortedResults[0][0]) ;?></p>
		        <div class="share-social">
		        	<?php
					    $title = ("Tengo un ".(int)($sortedResults[0][1])."% de compatibilidad con ".getCandidate($candidates,$sortedResults[0][0])->post_title);
					    $url = ( site_url()."/test/" );
					    $summary = ("Quieres saber con que candidato tienes más afinidad, dale click aquí y averigualo.");
					    $image = get_field('foto_share', getCandidate($candidates,$sortedResults[0][0])->ID);;
					?>
		        	<a href="javascript:;" 
		        		onclick="postToFeed('<?php echo $image ?>', '<?php echo $title; ?>', '<?php echo $summary; ?>');" class="share-icon">
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/facebook_share.png">
		        	</a>
		        	<a href="javascript:;" onclick="window.open('https://twitter.com/intent/tweet?url=<?php echo site_url()."/test/"; ?>&amp;text=<?php echo urlencode($title)." ".urlencode($twitter_summary); ?>&amp;via=gpselectoral','facebook-share-dialog','width=626,height=436')" class="share-icon" >
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/twitter_share.png">
		        	</a>
		        </div>
		    </div>  
		</div>
		 <script type="text/javascript">

	        function postToFeed(picture, title, summary){
				var obj = {method: 'feed',link: '<?php echo site_url()."/test/"; ?>', picture: picture,name: title, description: summary};
				function callback(response){}
				FB.ui(obj, callback);
			}

        </script>
		<!--
		<div class="promos responsive">  

		    <div class="promo third scale ganador">
		        <h4>63%</h4>
		        <img src="http://gpselectoral.pe/wp-content/uploads/2016/01/julio_guzman.jpg">
		        <h2>Julio Guzmán</h2>
		        <p>Todos por el Perú</p>
		    </div>  
		</div>
		
		-->
	</div>
	
	<section class="todos">
		<h2><span>Todos los candidatos</span></h2>
		
			<?php for($i = 3; $i < count($sortedResults); ++$i): ?>
				
					<article>
						<ul>
							<li>
								<!---->
								<img src="<?php echo get_field('foto_perfil', getCandidate($candidates,$sortedResults[$i][0])->ID); ?>">
							</li>
							<li class="puntuacion" style="width:<?php echo (int)($sortedResults[$i][1]); ?>%;"><?php echo (int)($sortedResults[$i][1]); ?>%</li>
							
						</ul>
					</article>
				
			<?php endfor; ?>
		

			
	</section>

	<!--Mobile Listado Candidatos -->
	

	<section class="todos Wrapper">
		<h2><span>Resumen de respuestas</span></h2>
		<?php $i = 1; ?>

		<?php 

            $cant_preguntas = query_posts(array(

                'post_type' => 'preguntas',

                'showposts' => 50,

                'orderby' => 'menu_order',

                'order'   => 'ASC'

            ) );  

            $cant_preguntas = count($cant_preguntas);
        ?>
        <h4 class="msj-comp">Compara tus posiciones con la de los candidatos</h4>
	        
        <div class="content-match" style="   overflow: visible;    position: relative;    opacity: 1;">
        	
			
					<div class="box-match ">

						<div class="side-left">
							<span class="bottom-txt">Tu opinión</span>
						</div>	

						<div class="side-right user-results">
						<?php $i = 0; ?>
							<?php foreach ($userQuestionResultsJSON as $key => $value): ?>
								<?php $i++; ?>
								<div >
									<div class="box-user">
										<?php $titulo_pregunta = get_post( $key )->post_title; ?>
										<p><span><?php echo $i; ?>. </span> <?php echo $titulo_pregunta ?></p>
										<?php $txt_res =""; ?>
         								<?php switch ( $value['opinion'] ) {
         									case 'de_acuerdo':
         										# code...
         										$txt_res ="De acuerdo";
         										break;
         									case 'neutral':
         										# code...
         										$txt_res ="Neutral";
         										break;
         									case 'desacuerdo':
         										# code...
         										$txt_res ="En desacuerdo";
         										break;
         									default:
         										$txt_res ="No respondió";
         										break;
         								} ?>
										<div class="opinion"><?php echo $txt_res; ?></div>
									</div>

								</div>

							<?php endforeach ?>

						</div>
						<?php //var_dump($userQuestionResultsJSON); ?>
					</div>
					<hr class="sep-res">
					<div class="box-match">
						
						<?php 
				        	
				        	global $wpdb;
							$results = $wpdb->get_results( 'SELECT id_candidato, preguntas, links, comentarios FROM candidato_preguntas', OBJECT );
				         	shuffle($results); ?>
				         	<?php //var_dump($results); ?>
				         	<div class="side-left">
					         	<?php foreach ($results as $key => $value) {


					         		$foto_candidato = get_field('foto_perfil', $value->id_candidato ); 
						         	//Imprimir los Candidatos que respondieron
						         	?>
						         		
						         		<aside class="pull-left">
				         					<img src="<?php echo $foto_candidato; ?>">
				         					
				         				</aside>
				         				
				         				
						        <?php } ?>
						    </div>
							<style type="text/css">
								.clear{
									clear: both;
								}
							</style>		    
						    <div class="side-right">

						    	<div class="slick-match">

						    		<?php //$preguntas_respondidas = json_decode( $results[0]->preguntas ); ?>
						    	
						    		<?php foreach ($userQuestionResultsJSON as $rp => $r) {  ?>


						    			<div class="preg-match pull-left" data-id="<?php echo $rp; ?>" >

								    		<?php foreach ($results as $key) {  ?>

													<?php $respuestas = json_decode($key->preguntas, true); ?>
													<?php $links = json_decode($key->links, true); ?>
													<?php $comments = json_decode($key->comentarios, true); ?>
						         					
													<?php foreach ($respuestas as $key2 => $val): ?>
															
															<?php if ($rp == $key2): ?>
							         							
							         							<?php $match = $val == $r['opinion'] ? "match":""; ?>

							         							<?php
							         								/*$link_youtube = json_decode($key2->links, true);
								         							if ( $link_youtube != null ) {
													         			$video = array_key_exists( $id_pregunta , $link_youtube ) ? $link_youtube[$id_pregunta]:"";
													         		} */
													         		//var_dump( $key2 );
													         		//var_dump( $links[$key2] );

									         						$parts = parse_url( $links[$key2] );
																	parse_str($parts['query'], $query);
																	$v = $query['v'];
																	//var_dump( $v );
												         		?>
								         						<aside class="pull-left <?php echo $match; ?> <?php echo $val."-".$r['opinion']; ?>" data-id="<?php echo $rp; ?>">		

								         							<p>
								         								<?php $txt_res =""; ?>
								         								<?php switch ( $val ) {
								         									case 'de_acuerdo':
								         										# code...
								         										$txt_res ="De acuerdo";
								         										break;
								         									case 'neutral':
								         										# code...
								         										$txt_res ="Neutral";
								         										break;
								         									case 'desacuerdo':
								         										# code...
								         										$txt_res ="En desacuerdo";
								         										break;
								         									default:
								         										$txt_res ="No respondió";
								         										break;
								         								} ?>
								         								<?php echo $txt_res; ?>
								         							</p>
								         							

										         					<?php if ( trim( $v ) != "") { ?>
											         					
											         					<a href="http://www.youtube.com/embed/<?php echo $v; ?>?autoplay=1" class="video-rpta fancybox.iframe">
											         						<span class="icon-video"></span>
											         						Explicación &#9656;
											         					</a>

																	<?php }else{ ?>

																		<a href="#inline" class="various-inline">Explicación &#9656;</a>
																		<div style="display:none;"><?php echo ( $comments[$rp] ); ?></div>
																	<?php } ?>
								         						</aside>
								         						
								         						<div class="clear"></div>

								         					<?php endif ?>

													<?php endforeach ?>

							         							
						         					
						         			<?php } ?>

										</div>

								    <?php } ?>
							    </div>

						    </div>
				    </div> 
					

					<?php $i++; ?>
				
			
<div id="inline"></div>
			
		</div>

	</section>

<?php get_footer(); ?>

